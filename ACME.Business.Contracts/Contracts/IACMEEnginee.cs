﻿using ACME.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME.Business.Contracts
{
    public interface IACMEEnginee
    {
        void NotificarProveedores();

        void NotificarClientes();
    }
}

