/**
 * Created by bayron.lara on 12/24/2015.
 */
(function () {
    "use strict";

    angular
        .module("pointOfSales")
        .controller("ShoppingCartCtrl",
                    ["products",
                     "invoice",
                     "invoiceResource",
                     "$scope",
                     "productService",
                     "categoryResource",
                     "$rootScope",
                     ShoppingCartCtrl]);

    function ShoppingCartCtrl(products, invoice, invoiceResource, $scope, productService, categoryResource, $rootScope) {


       
        $scope.form = {};

        $scope.invoiceReceipt = { invoiceId: ''};

        $scope.checkedOut = false;

        $scope.products = products;
        
        $scope.invoice = angular.copy(invoice);

        categoryResource.query(function (data) {
            $scope.categories = data;
        });

        $scope.importedTaxPercentage = $rootScope.importSalesTax;
        $scope.basicTaxPercentage = $rootScope.basicSalesTax;

        $scope.newInvoice = function () {
            $scope.checkedOut = false;
            $scope.invoice = angular.copy(invoice);
        }        

        $scope.addItem = function () {
            $scope.invoice.items.push({
                qty: 1
            });
        }

        $scope.removeItem = function (index) {
            $scope.invoice.items.splice(index, 1);
        }



        $scope.getInvoice = function () {
                        
            invoiceResource.get({ invoiceId: $scope.invoiceReceipt.invoiceId }, function (data) {
               
                $scope.invoice = data;
                $scope.invoiceReceipt.invoiceId = '';
                $scope.form.receiptForm.$setPristine();
            },
            function (response) {

                var errorMessage="";
                if (response.status == 404) {
                    errorMessage = "The invoice #" + $scope.invoiceReceipt.invoiceId + " was not found";                    
                    alert(errorMessage);
                    toastr.error(errorMessage);
                }
                else {
                    errorMessage = response.statusText + "\r\n";
                    if (response.data.modelState) {
                        for (var key in response.data.modelState) {
                            errorMessage += response.data.modelState[key] + "\r\n";
                        }
                    }
                    if (response.data.exceptionMessage)
                        errorMessage += response.data.exceptionMessage;

                    alert(errorMessage);
                    toastr.error(errorMessage);
                }

                $scope.invoiceReceipt.invoiceId = '';
                $scope.form.receiptForm.$setPristine();
            });
        }
     


        $scope.checkout = function () {

            //remove the empty items
            $scope.invoice.items = productService.removeEmptyInvoiceItems($scope.invoice.items);

           
            //set the flag
            $scope.checkedOut = true;
            
            //Save the invoice
            $scope.invoice.$save(
                function (data) {
                    $scope.invoice.invoiceId = data.invoiceId;
                    toastr.success("The invoice: #" + $scope.invoice.invoiceId + ", was successfully generated!");
                },
                function (response) {
                    var errorMessage = response.statusText + "\r\n";
                    if (response.data.modelState) {
                        for (var key in response.data.modelState) {
                            errorMessage += response.data.modelState[key] + "\r\n";
                        }
                    }
                    if (response.data.exceptionMessage)
                        errorMessage += response.data.exceptionMessage;

                    alert(errorMessage);
                    toastr.error(errorMessage);
                });

        }


        $scope.total = function () {
            var total = 0;
            var netTotal = 0;
          
            angular.forEach($scope.invoice.items, function (item) {
                if (item.product) {
                    item.product.price = productService.calculateProductPrice(item.product, $scope.categories, $scope.basicTaxPercentage, $scope.importedTaxPercentage);
                    total += item.qty * item.product.price;
                    netTotal += item.qty * item.product.netPrice;
                }               
            })

            $scope.invoice.salesTaxes = (total - netTotal);
            $scope.invoice.subTotal = netTotal;
            $scope.invoice.total = total;
            return total;
        }
    }
}());
