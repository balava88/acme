/**
 * Created by bayron.lara on 12/23/2015.
 */
(function () {
    "use strict";
    var app = angular.module("pointOfSales",
        ["common.services",
            "ui.router",
            "ui.mask",
            "ui.bootstrap",
            "cgBusy",
            "angular.chosen",
            "productResourceMock"]);

    app.config(function ($provide) {
        $provide.decorator("$exceptionHandler",
            ["$delegate",
                function ($delegate) {
                    return function (exception, cause) {
                        exception.message = "Please contact the Help Desk! \n Message: " +
                                                                exception.message;
                        $delegate(exception, cause);
                        alert(exception.message);
                    };
                }]);
    });

    app.config(["$stateProvider",
            "$urlRouterProvider",
            function ($stateProvider, $urlRouterProvider) {
                $urlRouterProvider.otherwise("/");

                $stateProvider
                    .state("home", {
                        url: "/",
                        templateUrl: "app/welcomeView.html"
                    })
                    // Products
                    .state("productList", {
                        url: "/products",
                        templateUrl: "app/products/productListView.html",
                        controller: "ProductListCtrl as vm"
                    })
                    .state("productEdit", {
                        abstract: true,
                        url: "/products/edit/:productId",
                        templateUrl: "app/products/productEditView.html",
                        controller: "ProductEditCtrl as vm",
                        resolve: {
                            productResource: "productResource",                            
                            product: function (productResource, $stateParams) {
                                var productId = $stateParams.productId;
                                return productResource.get({ productId: productId }).$promise;
                            }
                        }
                    })
                    .state("productEdit.info", {
                        url: "/info",
                        templateUrl: "app/products/productEditInfoView.html"
                    })
                    .state("productEdit.price", {
                        url: "/price",
                        templateUrl: "app/products/productEditPriceView.html"
                    })
                    .state("productEdit.image", {
                        url: "/image",
                        templateUrl: "app/products/productEditImageView.html"
                    })
                    .state("productDetail", {
                        url: "/products/:productId",
                        templateUrl: "app/products/productDetailView.html",
                        controller: "ProductDetailCtrl as vm",
                        resolve: {
                            productResource: "productResource",
                            categoryResource: "categoryResource",
                            categories: function (categoryResource) {
                                return categoryResource.query().$promise;
                            },
                            product: function (productResource, $stateParams) {
                                var productId = $stateParams.productId;
                                return productResource.get({ productId: productId }).$promise;
                            }
                        }
                    })
                    .state("shoppingCart", {
                        url: "/shoppingCart/invoice/:invoiceId",
                        templateUrl: "app/pos/shoppingCartView.html",
                        controller: "ShoppingCartCtrl",
                        resolve: {
                            productResource: "productResource",
                            invoiceResource: "invoiceResource",
                            invoice: function (invoiceResource, $stateParams) {
                                var invoiceId = $stateParams.invoiceId;
                                return invoiceResource.get({ invoiceId: invoiceId }).$promise;
                            },
                            products: function (productResource) {
                                return productResource.query(function(response) {
                                        // no code needed for success
                                    },
                                    function(response) {
                                        if (response.status == 404) {
                                            alert("Error accessing resource: " +
                                                response.config.method + " " +response.config.url);
                                        } else {
                                            alert(response.statusText);
                                        }
                                    }).$promise;

                            }
                        }
                    })
            }]
    );


    app.run(['$http', '$rootScope', function ($http, $rootScope) {

        $rootScope.basicSalesTax = 10;
        $rootScope.importSalesTax = 5;

        $http.get("/api/taxes/basic").success(function(response) {
            $rootScope.basicSalesTax = response.rate;
        });

        $http.get("/api/taxes/import").success(function (response) {
            $rootScope.importSalesTax = response.rate;
        });       

    }]);


    //app.constant("appSettings",
    //    {
    //        basicSalesTax: 10,
    //        importSalesTax: 5
    //    });


}());