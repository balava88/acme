/**
 * Created by bayron.lara on 12/23/2015.
 */
(function () {
    "use strict";

    angular
        .module("pointOfSales")
        .controller("ProductEditCtrl",
        ["product",
         "$state",
         "productService",
         "categoryResource",         
         "$rootScope",
         ProductEditCtrl]);


    function ProductEditCtrl(product, $state, productService, categoryResource, $rootScope) {

        var vm = this;

        categoryResource.query(function (data) {
            vm.categories = data;
        });
                
        
        vm.product = product;

        vm.importedTaxPercentage = $rootScope.importSalesTax;
        vm.basicTaxPercentage = $rootScope.basicSalesTax;


        if (vm.product && vm.product.imported) {
            vm.importOption = "imported";
        }
        else {
            vm.importOption = "notImported";
        }

        if (vm.product && vm.product.productId) {
            vm.title = "Edit: " + vm.product.productName;
        }
        else {
            vm.title = "New Product";
        }

     

        vm.isTaxable = function () {
            vm.calculatePrice();
            return productService.isCategoryTaxable(vm.product.categoryId, vm.categories);
        };

        vm.isImported = function () {
            vm.calculatePrice();
            vm.product.imported = (vm.importOption == 'imported');
            return vm.importOption == 'imported';
        };
        
        vm.taxAmount = function(){
            return productService.calculateMarginAmount(vm.product.price,
                                                        vm.product.netPrice)
        };

        /* Calculate the final price based on a Import duty and basic sales taxes */
        vm.calculatePrice = function () {
            var price = 0;
            var importedTaxAmount = 0;
            var basicTaxAmount = 0;

            price = vm.product.netPrice;


            if (productService.isCategoryTaxable(vm.product.categoryId, vm.categories)) {

                basicTaxAmount = productService.calculatePercent(vm.product.netPrice, vm.basicTaxPercentage);
                price += basicTaxAmount;
            }            

            if (vm.importOption == 'imported') {

                importedTaxAmount = productService.calculatePercent(vm.product.netPrice, vm.importedTaxPercentage);               
                price += importedTaxAmount;
            }

            price = (Math.round(price * 100)) / 100;


            vm.importedTaxAmount = importedTaxAmount;
            vm.basicTaxAmount = basicTaxAmount;
            vm.product.price = price;
        };

           

        vm.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            vm.opened = !vm.opened;
        };

        vm.submit = function (isValid) {
            if (isValid) {
                if (vm.product.productName && vm.product.productCode && vm.product.categoryId && vm.product.netPrice) {
                    
                    if (vm.product.productId) {

                        //update the product
                        vm.product.$update({ productId: vm.product.productId },
                            function (data) {
                                toastr.info("The product: " + vm.product.productName + ", was updated successfully!");
                            },
                            function (response) {
                                var errorMessage = response.statusText + "\r\n";
                                if (response.data.modelState) {
                                    for (var key in response.data.modelState) {
                                        errorMessage += response.data.modelState[key] + "\r\n";
                                    }
                                }
                                if (response.data.exceptionMessage)
                                    errorMessage += response.data.exceptionMessage;
                                  
                                alert(errorMessage);
                                toastr.error(errorMessage);
                            });
                    }
                    else {
                        //Save the product
                        vm.product.$save(
                            function (data) {
                                vm.product.productId = data.productId;
                                toastr.success("The product: " + vm.product.productName + ", was saved successfully!");
                            },
                            function (response) {
                                var errorMessage = response.statusText + "\r\n";
                                if (response.data.modelState) {
                                    for (var key in response.data.modelState) {
                                        errorMessage += response.data.modelState[key] + "\r\n";
                                    }
                                }
                                if (response.data.exceptionMessage)
                                    errorMessage += response.data.exceptionMessage;
                                  
                                alert(errorMessage);
                                toastr.error(errorMessage);
                            });
                    }


                } else {
                    alert("The product was not saved - Please enter the basic information first. (Name, Price, Category...)");
                    toastr.error("The product was not saved - Please enter the basic information first. (Name, Price, Category...)");
                }
            } else {                
                alert("Please correct the validation errors first.");
                toastr.error("Please correct the validation errors first.");
            }



        

        };

        vm.cancel = function () {
            $state.go('productList');
        };

       
       
    }
}());
