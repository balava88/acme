/**
 * Created by bayron.lara on 12/23/2015.
 */
(function () {
    "use strict";

    angular
        .module("pointOfSales")
        .controller("ProductDetailCtrl",
                    ["product",
                     "categories",
                     "productService",
                     ProductDetailCtrl]);

    function ProductDetailCtrl(product, categories, productService) {
        var vm = this;

        vm.product = product;
        vm.categories = categories;
        vm.title = "Product Detail: " + vm.product.productName; 
        vm.categoryName = productService.getCategoryNameById(vm.product.categoryId, vm.categories);

        if (vm.product.tags) {
            vm.product.tagList = vm.product.tags.toString();
        }
    }
}());
