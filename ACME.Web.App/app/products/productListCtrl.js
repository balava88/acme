/**
 * Created by bayron.lara on 12/23/2015.
 */
(function () {
    "use strict";
    angular
        .module("pointOfSales")
        .controller("ProductListCtrl",
                    ["productResource",
                        ProductListCtrl]);

    function ProductListCtrl(productResource) {
        var vm = this;

        productResource.query(function (data) {
            vm.products = data;
        });

        //vm.myPromise = productResource.query(function (data) {
        //    vm.products = data;
        //});


        vm.showImage = false;

        vm.toggleImage = function() {
            vm.showImage = !vm.showImage;
        }
    }
}());
