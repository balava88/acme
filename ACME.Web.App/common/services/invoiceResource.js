/**
 * Created by bayron.lara on 12/24/2015.
 */
(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("invoiceResource",
                ["$resource",
                 invoiceResource]);

    function invoiceResource($resource) {
        return $resource("/api/invoices/:invoiceId")
    }

}());
