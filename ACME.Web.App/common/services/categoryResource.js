/**
 * Created by bayron.lara on 12/23/2015.
 */
(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("categoryResource",
                ["$resource",
                 categoryResource]);

    function categoryResource($resource) {
        return $resource("/api/categories/:categoryId")
    }

}());
