/**
 * Created by bayron.lara on 12/23/2015.
 */
(function () {
    "use strict";

    var app = angular
                .module("productResourceMock",
                        ["ngMockE2E"]);

    app.run(function ($httpBackend) {
        var products = [
            {
                "productId": 1,
                "productName": "Package of headache pills",
                "productCode": "GDN0011",
                "releaseDate": "March 19, 2015",
                "description": "Bottle of aspirins - packet of headache pills.",
                "netPrice": 9.75,
                "price": 9.75,                
                "categoryId": 2,
                "imported": false,              
                "imageUrl": "http://www.senior-advisors.com/uploads/3/8/4/6/38465265/__885853_orig.png"
            },
            {
                "productId": 2,
                "productName": "Imported bottle of perfume",
                "productCode": "GDN0023",
                "releaseDate": "March 18, 2010",
                "description": "Cristian Dior - imported bottle of perfume",
                "netPrice": 27.99,
                "price": 32.19,                
                "categoryId": 5,
                "imported": true,           
                "imageUrl": "http://3.bp.blogspot.com/-UqZB3hF5wzk/UBRfL2SD8LI/AAAAAAAAbaE/5zmoU0j6pKI/s1600/PERFUMES+%252817%2529.png"
            },
            {
                "productId": 3,
                "productName": "Imported box of chocolates",
                "productCode": "TBX0048",
                "releaseDate": "May 21, 2013",
                "description": "Imported box of chocolates with a form of a heart - for Valentine's Day",
                "netPrice": 10.00,
                "price": 10.5,                
                "categoryId": 1,
                "imported": true,               
                "imageUrl": "http://www.officialpsds.com/images/thumbs/Heart-Box-Chocolates-psd98006.png"
            },
            {
                "productId": 4,
                "productName": "Chocolate Bar",
                "productCode": "TBX0022",
                "releaseDate": "May 15, 2009",
                "description": "Delicious chocolate bar",
                "netPrice": 0.85,
                "price": 0.85,                
                "categoryId": 1,
                "imported": false,               
                "imageUrl": "https://www.emojibase.com/resources/img/emojis/apple/x1f36b.png.pagespeed.ic.92CjJwDdkh.png"
            },
            {
                "productId": 5,
                "productName": "Music CD",
                "productCode": "GMG0042",
                "releaseDate": "October 15, 2002",
                "description": "80s disco music CD",
                "netPrice": 14.99,
                "price": 16.49,               
                "categoryId": 3,
                "imported": false,               
                "imageUrl": "http://icons.iconseeker.com/png/fullsize/hiddenmx-mac/music-cd-6.png"
            },
            {
                "productId": 6,
                "productName": "Book",
                "productCode": "BHG0042",
                "releaseDate": "October 15, 2002",
                "description": "Pholosophy book + relax music CD",
                "netPrice": 12.49,
                "price": 12.49,
                "categoryId": 4,
                "imported": false,
                "imageUrl": "http://oaktrust.library.tamu.edu/themes/Mirage2Ext//PrimerosLibros/imgs/blank_book.png"
            }
        ];


        var categories = [
          {
              "categoryId": 1,
              "categoryName": "Food",
              "taxable": false
          },
          {
              "categoryId": 5,
              "categoryName": "Beauty",
              "taxable": true
          },
          {
              "categoryId": 2,
              "categoryName": "Health / Medical",
              "taxable": false
          },
          {
              "categoryId": 3,
              "categoryName": "Technology",
              "taxable": true
          },
          {
              "categoryId": 4,
              "categoryName": "Books",
              "taxable": false
          },          
          {
              "categoryId": 6,
              "categoryName": "Home / Garden",
              "taxable": true
          },
          {
              "categoryId": 7,
              "categoryName": "Toys & Games",
              "taxable": true
          },
          {
              "categoryId": 8,
              "categoryName": "Sports & Outdoors",
              "taxable": true
          }

        ];

        var invoices = [];




        var invoiceUrl = "/api/invoices";

        var productUrl = "/api/products";

        var categoryUrl = "/api/categories";

        var basicTaxUrl = "/api/taxes/basic";

        var importTaxUrl = "/api/taxes/import";



        $httpBackend.whenGET(importTaxUrl).respond({ rate: 5 });

        $httpBackend.whenGET(basicTaxUrl).respond({rate: 10});


        $httpBackend.whenGET(productUrl).respond(products);

        $httpBackend.whenGET(invoiceUrl).respond(invoices);

        $httpBackend.whenGET(categoryUrl).respond(categories);

        var editingRegex = new RegExp(productUrl + "/[0-9][0-9]*", '');
        $httpBackend.whenGET(editingRegex).respond(function (method, url, data) {
            var product = {"productId": 0};
            var parameters = url.split('/');
            var length = parameters.length;
            var id = parameters[length - 1];

            if (id > 0) {
                for (var i = 0; i < products.length; i++) {
                    if (products[i].productId == id) {
                        product = products[i];
                        break;
                    }
                };
            }
            return [200, product, {}];
        });

        var invoiceEditingRegex = new RegExp(invoiceUrl + "/[0-9][0-9]*", '');
        $httpBackend.whenGET(invoiceEditingRegex).respond(function (method, url, data) {

            var statusCode = 200;
            var invoice = {
                "invoiceId": 0,
                "subTotal": 0,
                "salesTaxes": 0,
                "total": 0,
                "items": [{
                    qty: 1
                }]
            };
            var parameters = url.split('/');
            var length = parameters.length;
            var id = parameters[length - 1];

            if (id > 0) {
                for (var i = 0; i < invoices.length; i++) {
                    if (invoices[i].invoiceId == id) {
                        invoice = invoices[i];
                        break;
                    }
                };
            }

            
            if (id != 0 && invoice.invoiceId == 0) 
                statusCode = 404;


            return [statusCode, invoice, {}];
        });

        var categoryEditingRegex = new RegExp(categoryUrl + "/[0-9][0-9]*", '');
        $httpBackend.whenGET(categoryEditingRegex).respond(function (method, url, data) {
            var category = { "categoryId": 0 };
            var parameters = url.split('/');
            var length = parameters.length;
            var id = parameters[length - 1];

            if (id > 0) {
                for (var i = 0; i < categories.length; i++) {
                    if (categories[i].categoryId == id) {
                        category = categories[i];
                        break;
                    }
                };
            }
            return [200, category, {}];
        });

        $httpBackend.whenPOST(invoiceUrl).respond(function (method, url, data) {
            var invoice = angular.fromJson(data);

            if (!invoice.invoiceId) {
                // new invoice Id
                if (invoices.length==0) {
                    invoice.invoiceId = 1;
                } else {
                    invoice.invoiceId = invoices[invoices.length - 1].invoiceId + 1;                    
                }
                invoices.push(invoice);
                
                return [200, invoice, {}];
            }
            else {
                return [400, "Invalid invoice information", {}];
            }

        });


        $httpBackend.whenPOST(productUrl).respond(function (method, url, data) {
            var product = angular.fromJson(data);

            if (!product.productId) {
                // new product Id
                product.productId = products[products.length - 1].productId + 1;
                products.push(product);
                return [200, product, {}];
            }
            else {
                return [400, "Invalid product information", {}];
            }
            
        });


        var productPutEditingRegex = new RegExp(productUrl + "/[0-9][0-9]*", '');
        $httpBackend.whenPUT(productPutEditingRegex).respond(function (method, url, data) {
            var product = angular.fromJson(data);


            
            var parameters = url.split('/');
            var length = parameters.length;
            var id = parameters[length - 1];

            if (id > 0 && product.productId && id == product.productId) {
                // Updated product
                for (var i = 0; i < products.length; i++) {
                    if (products[i].productId == product.productId) {
                        products[i] = product;
                        break;
                    }
                };

                return [200, product, {}];
            } else {
                return [400, "Invalid product information", {}];
            }
        });


        // Pass through any requests for application files
        $httpBackend.whenGET(/app/).passThrough();


    })
}());
