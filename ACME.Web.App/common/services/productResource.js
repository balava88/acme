/**
 * Created by bayron.lara on 12/23/2015.
 */
(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("productResource",
                ["$resource",
                 productResource]);

    function productResource($resource) {
        return $resource("/api/products/:productId", null,
            {
                'update': {
                    method: 'PUT'
                }
            });
    }

}());
