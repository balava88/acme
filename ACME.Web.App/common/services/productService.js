/**
 * Created by bayron.lara on 12/23/2015.
 */
(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("productService",
                   productService);

    function productService(categoryResource) {
       
        
        function calculateMarginAmount(price, netPrice) {
            var margin = 0;
            if (price && netPrice) {
                margin = price - netPrice;
            }
            return margin;
        }
        function calculatePercent(value, percent) {
            var amount = 0;
            if (value && percent) {
                amount = (value * percent / 100);
                amount = (Math.round(amount * 100)) / 100;
            }
            return amount;
        }

        function basicSalesTaxIsApplied(categoryId, categories) {
            if (categories && categoryId) {
                for (var i in categories) {
                    if (categories[i].categoryId == categoryId) {
                        return categories[i].taxable;
                    }
                }
            }
            return false;
        }


        function removeEmptyInvoiceItems(invoiceItems) {

            var itemsWithValue = [];

            if (invoiceItems) {            
                for (var i in invoiceItems) {
                    if (invoiceItems[i].product) {
                        itemsWithValue.push(invoiceItems[i]);
                    }
                }     
            }

            return itemsWithValue;
        }


        function getCategoryNameById(categoryId, categories) {
            if (categories && categoryId) {
                for (var i in categories) {
                    if (categories[i].categoryId == categoryId) {
                        return categories[i].categoryName;
                    }
                }
            }
            return "";
        };

        function calculateProductPrice(product, categories, basicSalesTax, importedTax) {
            var productPrice = 0;
            var category = {};
            if (categories && product && basicSalesTax && importedTax) {

                for (var i in categories) {
                    if (categories[i].categoryId == product.categoryId) {
                        category = categories[i];
                        break;
                    }
                }

                productPrice = product.netPrice;

                if (category.taxable)
                    productPrice += calculatePercent(product.netPrice, basicSalesTax);              

                if (product.imported)
                    productPrice += calculatePercent(product.netPrice, importedTax);           

                productPrice = (Math.round(productPrice * 100)) / 100;
            }
            return productPrice;
        };
        

        return {
            
            calculateMarginAmount: calculateMarginAmount,
            calculatePercent: calculatePercent,
            isCategoryTaxable: basicSalesTaxIsApplied,
            getCategoryNameById: getCategoryNameById,
            removeEmptyInvoiceItems: removeEmptyInvoiceItems,
            calculateProductPrice: calculateProductPrice

          
        }

    }


}());
