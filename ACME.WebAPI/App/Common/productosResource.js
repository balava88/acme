﻿(function () {
    "use strict";

    angular.module("common.services")
           .factory("productosResource",
                    ["$resource",
                    productosResource]);

    function productosResource($resource) {
        return $resource("/api/productos/:id", null,
           {
               'update': {
                   method: 'PUT'
               },
               'query': {
                   method: 'GET',
                   isArray: true
               }
           });
    }


}());