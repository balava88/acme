﻿(function () {
    "use strict";

    angular.module("common.services")
           .factory("solicitudesResource",
                    ["$resource",
                     "currentUser",
                    solicitudesResource]);

    function solicitudesResource($resource, currentUser) {
        return $resource("/api/solicitudes/:id", null,
           {
               'query': {
                   isArray: true,
                   headers: { 'Authorization': 'Bearer ' + currentUser.getUserToken() }
               },

               'get': {
                   headers: { 'Authorization': 'Bearer ' + currentUser.getUserToken() }
               },

               'save': {
                   method: 'POST',
                   headers: { 'Authorization': 'Bearer ' + currentUser.getUserToken() }
               },

               'update': {
                   method: 'PUT',
                   headers: { 'Authorization': 'Bearer ' + currentUser.getUserToken() }
               }
           });
    }


}());