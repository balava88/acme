﻿(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("loggedUserAccount",
        [
            "$resource",
            "currentUser",
            loggedUserAccount
        ]);

    function loggedUserAccount($resource, currentUser) {
        return {

          
            user: $resource("/api/Account/Logout", null,
                    {
                        'logout': {
                            method: 'POST',
                            headers: { 'Authorization': 'Bearer ' + currentUser.getUserToken() }
                        }
                    }),
            change: $resource("/api/Account/ChangePassword", null,
                    {
                        'changePassword': {
                            method: 'POST',
                            headers: { 'Authorization': 'Bearer ' + currentUser.getUserToken() }
                        }
                    })
        }
    }


})();
