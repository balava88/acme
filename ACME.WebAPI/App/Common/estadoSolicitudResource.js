﻿(function () {
    "use strict";

    angular.module("common.services")
           .factory("estadoSolicitudResource",
                    ["$resource",
                    estadoSolicitudResource]);

    function estadoSolicitudResource($resource) {
        return $resource("/api/estadosSolicitud/", null,
           {
               
           });
    }


}());