﻿(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("userAccount",
        [
            "$resource",
            "appSettings",
            userAccount
        ]);

    function userAccount($resource, appSettings) {
        return {

            profile: $resource("/api/Account/UserInfo", null,
                    {
                        'getProfile': { method: 'GET' }
                    }),            
            reset: $resource("/api/Account/SetPassword", null,
                    {
                        'resetPassword': { method: 'POST' }
                    }),
            user: $resource("/api/Account/ActivateUser", null,
                    {
                        'activateUser': { method: 'POST' }
                    }),
            registration: $resource("/api/Account/Register", null,
                    {
                        'registerUser': { method: 'POST' }
                    }),
            login: $resource("/Token", null,
                    {
                        'loginUser': {
                            method: 'POST',
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                            transformRequest: function (data, headersGetter) {
                                var str = [];
                                for (var d in data)
                                    str.push(encodeURIComponent(d) + "=" +
                                                        encodeURIComponent(data[d]));
                                return str.join("&");
                            }

                        }
                    })
        }
    }


})();
