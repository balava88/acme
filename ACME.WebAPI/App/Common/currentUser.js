﻿(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("currentUser",
        [
            "$q",
            "$http",
            "$timeout",
            currentUser
        ]);

    function currentUser($q, $http, $timeout) {

        var _identity = undefined,
            _authenticated = false;

      

        var getUserToken = function () {
            return _authenticated ? _identity.token : '';             
        };

        var getUserFirstName = function () {
            return _authenticated ? _identity.firstName : '';
        };

        var getUserLastName = function () {
            return _authenticated ? _identity.lastName : '';
        };

        var getUserDocumentNumber = function () {
            return _authenticated ? _identity.documentNumber : '';
        };        

        var getUserApbName = function () {
            return _authenticated ? _identity.apbName : '';
        };

        var getUserIdApb = function () {
            return _authenticated ? _identity.idApb : 0;
        };

        var getUserName = function () {
            return _authenticated ? _identity.name : '';            
        };

        var isAuthenticated = function () {
            return _authenticated;
        };

        var isIdentityResolved = function () {
            return angular.isDefined(_identity);
        };

        var isInRole = function (role) {
            if (!_authenticated || !_identity.roles) {
                return false;
            }

            return _identity.roles.indexOf(role) != -1;
        };


        var authenticate = function (identity) {
            _identity = identity;
            _authenticated = identity != null;

            // for this demo, we'll store the identity in localStorage. For you, it could be a cookie, localStorage, whatever
            if (identity) {
                localStorage.setItem("ACME.identity", angular.toJson(identity));
            }
            else {
                localStorage.removeItem("ACME.identity");
            }
        };


        var identity = function (force) {
            var deferred = $q.defer();

            if (force === true) {
                _identity = undefined;
            }

            // check and see if we have retrieved the identity data from the server. if we have, reuse it by immediately resolving
            if (angular.isDefined(_identity)) {
                deferred.resolve(_identity);
                return deferred.promise;
            }



            // for the sake of the demo, we'll attempt to read the identity from localStorage. the example above might be a way if you use cookies or need to retrieve the latest identity from an api
            // i put it in a timeout to illustrate deferred resolution
            var self = this;
            $timeout(function () {
                _identity = angular.fromJson(localStorage.getItem("ACME.identity"));
                self.authenticate(_identity);
                deferred.resolve(_identity);
            }, 1000);

            return deferred.promise;
        };


        var isInAnyRole =  function(roles) {
            if (!_authenticated || !_identity.roles) {
                return false;
            }

            for (var i = 0; i < roles.length; i++) {
                if (this.isInRole(roles[i])) {
                    return true;
                }
            }

            return false;
        };

        return {
            getUserName: getUserName,
            getUserToken: getUserToken,
            identity: identity,
            isInRole: isInRole,
            isIdentityResolved: isIdentityResolved,
            authenticate: authenticate,
            isAuthenticated: isAuthenticated,
            isInAnyRole: isInAnyRole,
            getUserFirstName: getUserFirstName,
            getUserLastName: getUserLastName,
            getUserDocumentNumber: getUserDocumentNumber,
            getUserApbName: getUserApbName,
            getUserIdApb: getUserIdApb
            
        }
    }
})();
