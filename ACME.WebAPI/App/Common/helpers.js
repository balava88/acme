﻿(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("validator", validator);

    angular
        .module("common.services")
        .factory("viewModelHelper",
        [
            "$http",
            viewModelHelper
        ]);

   
    function viewModelHelper($http) {
        return ACME.viewModelHelper($http);
    }

    function validator() {
        return ACME.validator();
    }


})();




window.ACME = {};


(function (cr) {
    var viewModelHelper = function ($http) {

        var self = this;

        self.modelIsValid = true;
        self.modelErrors = [];
        self.isLoading = false;

        self.apiGet = function (uri, data, success, failure, always) {
            self.isLoading = true;
            self.modelIsValid = true;
            $http.get(CarRental.rootPath + uri, data)
                .then(function (result) {
                    success(result);
                    if (always != null)
                        always();
                    self.isLoading = false;
                }, function (result) {
                    if (failure == null) {
                        if (result.status != 400)
                            self.modelErrors = [result.status + ':' + result.statusText + ' - ' + result.data.Message];
                        else
                            self.modelErrors = [result.data.Message];
                        self.modelIsValid = false;
                    }
                    else
                        failure(result);
                    if (always != null)
                        always();
                    self.isLoading = false;
                });
        }

        self.apiPost = function (uri, data, success, failure, always) {
            self.isLoading = true;
            self.modelIsValid = true;
            $http.post(CarRental.rootPath + uri, data)
                .then(function (result) {
                    success(result);
                    if (always != null)
                        always();
                    self.isLoading = false;
                }, function (result) {
                    if (failure == null) {
                        if (result.status != 400)
                            self.modelErrors = [result.status + ':' + result.statusText + ' - ' + result.data.Message];
                        else
                            self.modelErrors = [result.data.Message];
                        self.modelIsValid = false;
                    }
                    else
                        failure(result);
                    if (always != null)
                        always();
                    isLoading = false;
                });
        }

        self.setErrorMessageByResponse = function (message, response) {   

            message.isError = true;

            switch (response.status) {
                case 400:
                    message.errorMessage = "Solicitud invalida";
                    break;
                case 500:
                    message.errorMessage = "Error interno";
                    break;
                case 404:
                    message.errorMessage = "Recurso no encontrado";
                    break;
                case 401:
                    message.errorMessage = "No autorizado";
                    break;
                default:
                    message.errorMessage = response.statusText;
            }

            message.errorDescription = "";

            if (response.data.message && !response.data.modelState)
                message.errorDescription += response.data.message;

            if (response.data.exceptionMessage)
                message.errorDescription += response.data.exceptionMessage;

            if (response.data.innerException && response.data.innerException.exceptionMessage)
                message.errorDescription += response.data.innerException.exceptionMessage;

            if (response.data.modelState)
                message.errors = response.data.modelState;


        }

        self.setErrorMessage = function (message, messageText, messageDescription, messageStates) {

            message.isError = true;
            message.errorMessage = messageText;           

            if (messageDescription)
                message.errorDescription = messageDescription;

            if (messageStates)
                message.errors = messageStates;
        }

        self.cleanMessages = function (message) {
            message.isError = false;
            message.errorMessage = "";
            message.errorDescription = "";
            message.errors = null;
            message.successMessage = "";
        }

        self.setSuccessMessage = function (message, text) {

            self.cleanMessages(message);

            if (text)
                message.successMessage = text;
            else
                message.successMessage = "Operación exitosa";

        }       

        return this;
    }
    cr.viewModelHelper = viewModelHelper;
}(window.ACME));


(function (val) {
    var validator = function () {

        var self = this;

        self.PropertyRule = function (propertyName, rules) {
            var self = this;
            self.PropertyName = propertyName;
            self.Rules = rules;
        };

        self.ValidateModel = function (model, allPropertyRules) {
            var errors = [];
            var props = Object.keys(model);
            for (var i = 0; i < props.length; i++) {
                var prop = props[i];
                for (var j = 0; j < allPropertyRules.length; j++) {
                    var propertyRule = allPropertyRules[j];
                    if (prop == propertyRule.PropertyName) {
                        var propertyRules = propertyRule.Rules;

                        var propertyRuleProps = Object.keys(propertyRules);
                        for (var k = 0; k < propertyRuleProps.length; k++) {
                            var propertyRuleProp = propertyRuleProps[k];
                            if (propertyRuleProp != 'custom') {
                                var rule = rules[propertyRuleProp];
                                var params = null;
                                if (propertyRules[propertyRuleProp].hasOwnProperty('params'))
                                    params = propertyRules[propertyRuleProp].params;
                                var validationResult = rule.validator(model[prop], params);
                                if (!validationResult) {
                                    errors.push(getMessage(prop, propertyRuleProp, rule.message));
                                }
                            }
                            else {
                                var validator = propertyRules.custom.validator;
                                var value = null;
                                if (propertyRules.custom.hasOwnProperty('params')) {
                                    value = propertyRules.custom.params;
                                }
                                var result = validator(model[prop], value());
                                if (result != true) {
                                    errors.push(getMessage(prop, propertyRules.custom, 'Invalid value.'));
                                }
                            }
                        }
                    }
                }
            }

            model['errors'] = errors;
            model['isValid'] = (errors.length == 0);
        }

        var getMessage = function (prop, rule, defaultMessage) {
            var message = '';
            if (rule.hasOwnProperty('message'))
                message = rule.message;
            else
                message = prop + ': ' + defaultMessage;
            return message;
        }

        var rules = [];

        var setupRules = function () {

            rules['required'] = {
                validator: function (value, params) {
                    return !(value.trim() == '');
                },
                message: 'Value is required.'
            };
            rules['minLength'] = {
                validator: function (value, params) {
                    return !(value.trim().length < params);
                },
                message: 'Value does not meet minimum length.'
            };
            rules['pattern'] = {
                validator: function (value, params) {
                    var regExp = new RegExp(params);
                    return !(regExp.exec(value.trim()) == null);
                },
                message: 'Value must match regular expression.'
            };
        }

        setupRules();

        return this;
    }
    val.validator = validator;
}(window.ACME));
