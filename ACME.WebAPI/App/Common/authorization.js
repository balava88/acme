﻿(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("authorization",
        [
            "$rootScope",
            "$state",
            "currentUser",
            authorization
        ]);

   

    function authorization($rootScope, $state, currentUser) {

        return {
            authorize: function () {
                return currentUser.identity()
                  .then(function () {
                      var isAuthenticated = currentUser.isAuthenticated();

                      if ($rootScope.toState.data.roles && $rootScope.toState.data.roles.length > 0 && !currentUser.isInAnyRole($rootScope.toState.data.roles)) {
                          if (isAuthenticated) {
                              $state.go('site.accessdenied'); // user is signed in but not authorized for desired state
                          }
                          else {
                              // user is not authenticated. stow the state they wanted before you
                              // send them to the signin state, so you can return them when you're done
                              $rootScope.returnToState = $rootScope.toState;
                              $rootScope.returnToStateParams = $rootScope.toStateParams;

                              // now, send them to the signin state so they can log in
                              $state.go('site.login');
                          }
                      }
                  });
            }
        };

    }


})();