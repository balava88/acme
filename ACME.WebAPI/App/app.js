(function () {
    "use strict";

    var app = angular.module("ACMEApp",
                            ["common.services",
                             "ngSanitize",
                             "ui.mask",
                             "ui.bootstrap",
                             "ui.router",
                             "ui.select",
                             "cgBusy",
                             "vcRecaptcha"]);


    app.config(function ($provide) {
        $provide.decorator("$exceptionHandler",
            ["$delegate",
                function ($delegate) {
                    return function (exception, cause) {
                        exception.message = "Por favor contacte al equipo de soporte! \n Mensaje: " + exception.message;
                        $delegate(exception, cause);
                        alert(exception.message);
                        toastr.error(exception.message);
                    };
                }]);
    });


    app.config(["$stateProvider",
           "$urlRouterProvider",
           function ($stateProvider, $urlRouterProvider) {

               $urlRouterProvider.otherwise("/site/login");

               $stateProvider
                   //sitio global -abstract
                   .state("site", {
                       abstract: true,
                       url: "/site",
                       templateUrl: "App/siteView.html",
                       resolve: {
                           authorize: [
                               'authorization',
                               function(authorization) {
                                   return authorization.authorize();
                               }
                           ]
                       }
                   })


                   //reinicio de clave
                   .state("site.resetPassword", {
                       url: "/resetPassword",
                       data: {
                           roles: []
                       },
                       templateUrl: "App/Login/resetPasswordView.html",
                       controller: "ResetPasswordCtrl as vm"
                   })

                   //registro de usuario
                   .state("site.register", {
                       url: "/register",
                       data: {
                           roles: []
                       },
                       templateUrl: "App/Login/registerView.html",
                       controller: "RegisterCtrl as vm"
                   })

                   // login
                   .state("site.login", {
                       url: "/login",
                       data: {
                           roles: []
                       },
                       templateUrl: "App/Login/loginView.html",
                       controller: "LoginCtrl as vm"
                   })

                   // acceso denegado
                   .state("site.accessdenied", {
                       url: "/accessdenied",
                       data: {
                           roles: []
                       },
                       templateUrl: "App/Login/accessDeniedView.html"
                   })


                   // activaci�n de usuario
                   .state("site.activateUser", {
                       url: "/activateUser/:userId",
                       data: {
                           roles: []
                       },
                       templateUrl: "App/Login/activateUserView.html",
                       controller: "ActivateUserCtrl as vm",
                       resolve: {
                           userAccount: "userAccount",
                           result: function(userAccount, $stateParams) {
                               var userId = $stateParams.userId;
                               return userAccount.user.activateUser({ userId: userId }).$promise;
                           }
                       }

                   })


                   //Dash board - area de trabajo -abstract
                   .state("site.dashboard", {
                       abstract: true,
                       url: "/dashboard",
                       templateUrl: "App/Dashboard/mainView.html",
                       controller: "MainCtrl as vm"
                   })

                   // Pantalla de bienvenida del dashboard
                   .state("site.dashboard.welcome", {
                       url: "/welcome",
                       data: {
                           roles: ['User']
                       },
                       templateUrl: "App/Dashboard/Welcome/welcomeView.html"

                   })

                   //Administrador de cuenta
                   .state("site.dashboard.accountEdit", {
                       url: "/account",
                       data: {
                           roles: ['User']
                       },
                       templateUrl: "App/Dashboard/MyAccount/accountEditView.html",
                       controller: "AccountEditCtrl as vm"

                   })



                   //Productos//------------

                   //Lista de productos
                   .state("site.dashboard.productList", {
                       url: "/products",
                       data: {
                           roles: ['User', 'Admin']
                       },
                       templateUrl: "App/Dashboard/Requests/productList.html",
                       controller: "ProductListCtrl as vm"
                   })


                   //detalle del producto
                   .state("site.dashboard.productDetail", {
                       url: "/product/detail/:productId",
                       data: {
                           roles: ['User', 'Admin']
                       },
                       templateUrl: "App/Dashboard/Requests/productDetailView.html",
                       controller: "ProductDetailCtrl as vm",
                       resolve: {
                           productosResource: "productosResource",
                           product: function(productosResource, $stateParams) {
                               var productId = $stateParams.productId;
                               return productosResource.get({ id: productId }).$promise;
                           }
                       }
                   })


                    //wizard creaci�n de productos
                   .state("site.dashboard.productEdit", {
                       abstract: true,
                       url: "/product/edit/:productId",
                       templateUrl: "App/Dashboard/Requests/productEditView.html",
                       controller: "ProductEditCtrl as vm",
                       resolve: {
                           productosResource: "productosResource",
                           product: function (productosResource, $stateParams) {
                               var productId = $stateParams.productId;
                               return productosResource.get({ id: productId }).$promise;
                           }
                       }
                   })


                   .state("site.dashboard.productEdit.info", {
                       url: "/info",
                       data: {
                           roles: ['User']
                       },
                       templateUrl: "App/Dashboard/Requests/productEditInfoView.html"
                   })

                   .state("site.dashboard.productEdit.image", {
                       url: "/image",
                       data: {
                           roles: ['User']
                       },
                       templateUrl: "App/Dashboard/Requests/productEditImageView.html"
                   })


                   //Solicitudes//----------------------------

                    //Lista de solicitudes
                   .state("site.dashboard.requestsList", {
                       url: "/requests",
                       data: {
                           roles: ['User']
                       },
                       templateUrl: "App/Dashboard/Requests/requestsListView.html",
                       controller: "RequestsListCtrl as vm",
                       resolve: {
                           estadoSolicitudResource: "estadoSolicitudResource",
                           solicitudesResource: "solicitudesResource",
                           estados: function (estadoSolicitudResource) {
                               return estadoSolicitudResource.query().$promise;
                           },
                           solicitudes: function (solicitudesResource) {
                               return solicitudesResource.query().$promise;
                            }
                       }
                   })


                    //Detalle de la solicitud
                   .state("site.dashboard.requestDetail", {
                       url: "/request/detail/:requestId",
                       data: {
                           roles: ['User']
                       },
                       templateUrl: "App/Dashboard/Requests/requestDetailView.html",
                       controller: "RequestDetailCtrl as vm",
                       resolve: {
                           solicitudesResource: "solicitudesResource",
                           productosResource: "productosResource",
                           estadoSolicitudResource: "estadoSolicitudResource",
                           request: function (solicitudesResource, $stateParams) {
                               var requestId = $stateParams.requestId;
                               return solicitudesResource.get({ id: requestId }).$promise;

                           },
                           estados: function (estadoSolicitudResource) {
                               return estadoSolicitudResource.query().$promise;
                           },
                           products: function (productosResource) {
                               return productosResource.query().$promise;
                           }
                       }
                   })

                   //Nueva solicitud
                   .state("site.dashboard.requestEdit", {
                       url: "/request/edit/:requestId",
                       data: {
                           roles: ['User']
                       },
                       templateUrl: "App/Dashboard/Requests/requestEditView.html",
                       controller: "RequestEditCtrl as vm",
                       resolve: {
                           solicitudesResource: "solicitudesResource",
                           productosResource: "productosResource",
                           request: function (solicitudesResource, $stateParams) {
                               var requestId = $stateParams.requestId;
                               return solicitudesResource.get({ id: requestId }).$promise;
                           },
                           products: function (productosResource) {
                               return productosResource.query().$promise;
                           }
                       }
                   });


           }]
    );


    app.run(['$rootScope', '$state', '$stateParams', 'authorization', 'currentUser',
        function ($rootScope, $state, $stateParams, authorization, currentUser) {
            $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {
                $rootScope.toState = toState;
                $rootScope.toStateParams = toStateParams;

                if (currentUser.isIdentityResolved()) {
                    authorization.authorize();
                }
            });
        }
    ]);


    /**
     * AngularJS default filter with the following expression:
     * "person in people | filter: {name: $select.search, age: $select.search}"
     * performs a AND between 'name: $select.search' and 'age: $select.search'.
     * We want to perform a OR.
     */
    app.filter('propsFilter', function () {
        return function (items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function (item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    });





}());