﻿(function () {
    "use strict";

    angular
        .module("ACMEApp")
        .controller("ResetPasswordCtrl",
                    ["userAccount",                     
                     "viewModelHelper",
                     ResetPasswordCtrl]);

    function ResetPasswordCtrl(userAccount, viewModelHelper) {
        var vm = this;

        vm.message = {};      

        vm.resetPassword = function () {
          
            vm.myPromise = userAccount.reset.resetPassword({ email: vm.email },
                function (data) {
                    vm.email = '';
                    viewModelHelper.setSuccessMessage(vm.message, "... Reinicio de clave exitoso. ");
                    toastr.success("... Reinicio de clave exitoso.");
                    toastr.success("En unos minutos recibirá un correo electrónico con las instrucciones para ingresar a la aplicación.");                    
                },
                function (response) {                   
                    
                    //viewModelHelper.setErrorMessage(vm.message, "Ejemplo", "Esta es la descripción", ["Hacha", "Calabaza", "Miel"]);
                    viewModelHelper.setErrorMessageByResponse(vm.message, response);
                 
                });
        }
      
    }
})();
