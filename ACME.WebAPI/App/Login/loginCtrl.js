﻿(function () {
    "use strict";

    angular
        .module("ACMEApp")
        .controller("LoginCtrl",
                    ["userAccount",
                     "currentUser",
                     "$state",
                     LoginCtrl]);

    function LoginCtrl(userAccount, currentUser, $state) {
        var vm = this;

        

        vm.message = '';

        vm.userData = {
            userName: '',
            email: '',
            password: '',
            confirmPassword: ''
        };

  

        vm.login = function () {
            vm.userData.grant_type = "password";
            vm.userData.userName = vm.userData.email;

            vm.myPromise = userAccount.login.loginUser(vm.userData,
                            function (data) {
                                vm.message = "";
                                vm.userData.password = "";
                                vm.access_token = data.access_token;

                            vm.myPromise = userAccount.profile.getProfile({ userName: vm.userData.userName },
                                    function (profile) {

                                        if (profile.isLockOut) {                                            
                                            vm.message = "Su cuenta no ha sido activada. \r\n";
                                        } else {

                                            var rolesArray = profile.userRoles.split(',');

                                            currentUser.authenticate({
                                                firstName: profile.firstName,
                                                lastName: profile.lastName,
                                                documentNumber: profile.documentNumber,
                                                idApb: profile.idApb,
                                                apbName: profile.apbName,
                                                name: vm.userData.userName,
                                                token: vm.access_token,
                                                roles: rolesArray
                                            });

                                            $state.go('site.dashboard.welcome');
                                        }
                                    },
                                    function (response) {

                                        vm.message = response.statusText + "\r\n";
                                        if (response.data.exceptionMessage)
                                            vm.message += response.data.exceptionMessage;
                                        if (response.data.error)
                                            vm.message += response.data.error;
                                        if (response.data.error_description)
                                            vm.message = response.data.error_description;
                                        
                                    });

                                                                
                                
                            },
                            function (response) {
                                vm.userData.password = "";

                                if (response.status == 400 && response.data.error && response.data.error == "invalid_grant") {
                                    vm.message = "Usuario y/o contraseña invalidos \r\n";
                                }else {
                                    vm.message = response.statusText + "\r\n";
                                    if (response.data.exceptionMessage)
                                        vm.message += response.data.exceptionMessage;
                                    if (response.data.error) 
                                        vm.message += response.data.error; 
                                    if (response.data.error_description)
                                        vm.message = response.data.error_description;
                                }                    
                            });
        }
    }
})();
