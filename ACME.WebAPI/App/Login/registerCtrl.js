﻿(function () {
    "use strict";

    angular
        .module("ACMEApp")
        .controller("RegisterCtrl",
                    ["userAccount",
                     "$state",
                     "viewModelHelper",
                     RegisterCtrl]);

    function RegisterCtrl(userAccount, $state, viewModelHelper) {
        var vm = this;

        //vm.isLoggedIn = function () {
        //    return currentUser.getProfile().isLoggedIn;
        //};

        vm.message = {};



        vm.userData = {
            firstName: '',
            lastName: '',
            document: '',
            email: '',
            password: '',
            confirmPassword: ''
        };

        vm.clean = function () {
            vm.userData.firstName = '';
            vm.userData.lastName = '';
            vm.userData.document = '';
            vm.userData.email = '';
            vm.userData.password = '';
            vm.userData.confirmPassword = '';
        };

      

        vm.registerUser = function () {
          
            vm.myPromise = userAccount.registration.registerUser(vm.userData,
                function (data) {                    
                    viewModelHelper.setSuccessMessage(vm.message, "... Registro de usuario exitoso. ");
                    toastr.success("... Registro de usuario exitoso.");
                    vm.clean();
                },
                function (response) {
                    //viewModelHelper.setErrorMessage(vm.message, "Ejemplo", "Esta es la descripción", ["Hacha", "Calabaza", "Miel"]);
                    viewModelHelper.setErrorMessageByResponse(vm.message, response);
                 
                });
        }

       
    }
})();
