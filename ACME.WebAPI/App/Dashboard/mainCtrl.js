﻿(function () {
    "use strict";

    angular
        .module("ACMEApp")
        .controller("MainCtrl",
                    ["currentUser",
                     "$state",
                     "loggedUserAccount",
                     "viewModelHelper",
                     MainCtrl]);

    function MainCtrl(currentUser, $state, loggedUserAccount, viewModelHelper) {

        var vm = this;

        vm.message = {};

        vm.isAdmin = function () {
            return currentUser.isInRole('Admin');
        };

        vm.userName = currentUser.getUserName();

        vm.signOut = function () {            

            vm.myPromise = loggedUserAccount.user.logout(
               function (data) {
                   //$state.go('site.login'); 
                   currentUser.authenticate(null);
                   window.location.href = '/';
               },
               function (response) {                   
                   viewModelHelper.setErrorMessageByResponse(vm.message, response);
               });
        };

    }
})();