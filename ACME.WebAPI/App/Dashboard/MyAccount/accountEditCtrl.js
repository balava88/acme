﻿(function () {
    "use strict";

    angular
        .module("ACMEApp")
        .controller("AccountEditCtrl",
                    ["currentUser",
                    "$state",
                    "$scope",
                    "loggedUserAccount",
                    "viewModelHelper",
                    AccountEditCtrl]);

    function AccountEditCtrl(currentUser, $state, $scope, loggedUserAccount, viewModelHelper) {
        var vm = this;
        //vm.isLoggedIn = function () {
        //    return currentUser.getProfile().isLoggedIn;
        //};

        vm.title = "Cuenta de usuario";
        vm.userName = currentUser.getUserName();
        vm.name = currentUser.getUserFirstName() + ' ' + currentUser.getUserLastName();
        vm.document = currentUser.getUserDocumentNumber();
        vm.company = currentUser.getUserApbName();

        vm.message = {};

        vm.clean = function () {
            vm.currentPassword = '';
            vm.newPassword = '';
            vm.confirmNewPassword = '';
            $scope.resetPasswordForm.$setPristine();
        };


        vm.changePassword = function (valid) {
       
            if (valid) {

                vm.myPromise = loggedUserAccount.change.changePassword(
                    { oldPassword: vm.currentPassword, newPassword: vm.newPassword, confirmPassword: vm.confirmNewPassword },
                    function (data) {
                        viewModelHelper.setSuccessMessage(vm.message, "... Su contraseña ha sido modificada con exito. ");
                        toastr.success("Su contraseña ha sido modificada con exito.");
                        vm.clean();
                    },
                    function (response) {
                        viewModelHelper.setErrorMessageByResponse(vm.message, response);
                        vm.clean();
                    }
                );

                          
                
            } else {
                toastr.error("No se puede cambiar la contraseña, revise la información ingresada.");
            }
        };


    }
})();
