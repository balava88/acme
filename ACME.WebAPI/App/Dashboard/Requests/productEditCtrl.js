﻿(function () {
    "use strict";

    angular
        .module("ACMEApp")
        .controller("ProductEditCtrl",
                    ["product",
                     "currentUser",
                     "$state",
                     "viewModelHelper",
                     ProductEditCtrl]);

    function ProductEditCtrl(product, currentUser, $state, viewModelHelper) {

        var vm = this;
        vm.message = {};
        vm.product = product;
        

        if (vm.product && vm.product.id_producto) {
            vm.title = "Modificar producto: #" + vm.product.id_producto;
        }
        else {
            vm.title = "Nueva producto";
        }
      
       
        vm.cancel = function () {
            $state.go('site.dashboard.productList');
        };


        vm.submit = function (isValid) {
            if (isValid) {
                if (vm.product.nombre_producto
                    && vm.product.codigo
                    && vm.product.descripcion
                    && vm.product.imagen
                   ) {

                    if (vm.product.id_producto) {

                        //update the product
                        vm.product.$update({ id: vm.product.id_producto },
                            function (data) {
                                toastr.success("Su producto ha sido actualizado de manera exitosa!");
                            },
                            function (response) {
                                viewModelHelper.setErrorMessageByResponse(vm.message, response);
                            });
                    }
                    else {
                        //Save the product
                        vm.myPromise = vm.product.$save(
                            function (data) {
                                vm.product.id_producto = data.id_producto;
                                toastr.success("Su producto ha sido guardado de manera exitosa!");
                                $state.go('site.dashboard.requestProductList');
                            },
                            function (response) {
                                viewModelHelper.setErrorMessageByResponse(vm.message, response);
                            });
                    }
                } else {
                    alert("El producto no pudo ser guardado - Por favor revise la información.");
                    toastr.error("El producto no pudo ser guardado - Por favor revise la información.");
                }
            } else {
                alert("El producto no pudo ser guardado - Por favor revise los errores del formulario.");
                toastr.error("El producto no pudo ser guardado - Por favor revise los errores del formulario.");
            }            
        };
       

        vm.isLoggedIn = function () {
            return currentUser.getProfile().isLoggedIn;
        };


    }
})();