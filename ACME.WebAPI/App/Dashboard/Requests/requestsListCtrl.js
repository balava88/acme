﻿(function () {
    "use strict";

    angular
        .module("ACMEApp")
        .controller("RequestsListCtrl",
                    ["currentUser",
                     "viewModelHelper",
                     "estados",
                     "solicitudes",
                     "solicitudesResource",
                     RequestsListCtrl]);

    function RequestsListCtrl(currentUser, viewModelHelper, estados, solicitudes, solicitudesResource) {
        var vm = this;
        vm.message = {};
        vm.estados = estados;
        vm.solicitudes = solicitudes;

        vm.loadRequest = function () {

            vm.myPromise = solicitudesResource.query( function (data) {
                vm.requests = data;
            }).$promise;

        }


        vm.loadRequest();
       
        vm.getEstadoSolicitud = function (solicitudes, est) {
            if (solicitudes && est) {
                for (var i = 0; i < solicitudes.length; i++) {
                    for (var j = 0; j < est.length; j++) {
                        if (solicitudes[i].id_estado_solicitud == est[j].id_estado_solicitud) {
                            return est[j].nombre_estado;
                        }
                    }
                }
            }
            return "";
        };

        vm.estadoSolicitud = vm.getEstadoSolicitud(vm.solicitudes, estados);
    }
})();