﻿(function () {
    "use strict";

    angular
        .module("ACMEApp")
        .controller("RequestEditCtrl",
                    ["request",
                     "products",
                     "$state",
                     "viewModelHelper",
                     RequestEditCtrl]);

    function RequestEditCtrl(request, products, $state, viewModelHelper) {

        var vm = this;
        vm.message = {};
        vm.request = request;
        vm.request.time = 1;
        vm.products = products;
        vm.title = "Detalle solicitud: #" + vm.request.id_solicitud;

        vm.addItem = function () {
            vm.request.solicitud.push({
                cantidad: 1
            });
        }

        vm.removeItem = function (index) {
            vm.request.solicitud.splice(index, 1);
        }

        function removeEmptyInvoiceItems(items) {

            var itemsWithValue = [];

            if (items) {
                for (var i in items) {
                    if (items[i].id_producto) {
                        itemsWithValue.push(items[i]);
                    }
                }
            }

            return itemsWithValue;
        }


        vm.createRequest = function () {

            //remove the empty items
            vm.request.solicitud = removeEmptyInvoiceItems(vm.request.solicitud);
          

            //Save the request
            vm.request.$save(
                function (data) {
                    vm.request.id_solicitud = data.id_solicitud;
                    toastr.success("La solicitud: #" + vm.request.id_solicitud + ", se genero de forma exitosa!");
                    $state.go('site.dashboard.requestsList');
                },
                function (response) {
                    viewModelHelper.setErrorMessageByResponse(vm.message, response);
                });

        }


        vm.addItem();


    }
})();
