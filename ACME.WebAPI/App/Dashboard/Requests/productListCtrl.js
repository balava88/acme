﻿(function () {
    "use strict";

    angular
        .module("ACMEApp")
        .controller("ProductListCtrl",
                    ["$state",
                    "productosResource",
                    "$scope",
                    "currentUser",
                    ProductListCtrl]);

    function ProductListCtrl($state, productosResource, $scope, currentUser) {

        var vm = this;
        vm.showImage = true;

        vm.isAdmin = function () {
            return currentUser.isInRole('Admin');
        }

        vm.toggleImage = function () {
            vm.showImage = !vm.showImage;
        }

        vm.loadRequest = function () {

            vm.myPromise = productosResource.query( function (data) {
                vm.requests = data;
            }).$promise;

        }


        vm.loadRequest();
    }
})(); 