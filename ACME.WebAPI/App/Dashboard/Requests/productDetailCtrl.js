﻿(function () {
    "use strict";

    angular
        .module("ACMEApp")
        .controller("ProductDetailCtrl",
                    ["product",
                     "productosResource",
                     ProductDetailCtrl]);

    function ProductDetailCtrl(product, productosResource) {
        var vm = this;

        vm.product = product;
    }

}());
