﻿(function () {
    "use strict";

    angular
        .module("ACMEApp")
        .controller("RequestDetailCtrl",
                    ["request",
                     "products",
                     "estados",
                     RequestDetailCtrl]);

    function RequestDetailCtrl(request, products, estados) {

        var vm = this;
        vm.message = {};
        vm.request = request;
        vm.products = products;
        vm.estados = estados;
        vm.title = "Detalle solicitud: #" + vm.request.id_solicitud;

        vm.getProductNameById = function (id, productos) {
            if (productos && id) {
                for (var i in productos) {
                    if (productos[i].id_producto == id) {
                        return productos[i].nombre_producto;
                    }
                }
            }
            return "";
        };

        vm.getEstadoSolicitud = function (id, estados) {
            if (estados && id) {
                for (var i in estados) {
                    if (estados[i].id_estado_solicitud == id) {
                        return estados[i].nombre_estado;
                    }
                }
            }
            return "";
        };
        vm.estadoSolicitud = vm.getEstadoSolicitud(vm.request.id_estado_solicitud, vm.estados);
    }
})();
