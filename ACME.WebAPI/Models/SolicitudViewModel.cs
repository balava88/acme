﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ACME.Entities;

namespace ACME.WebAPI.Models
{
    public class SolicitudViewModel
    {
        public int Id_solicitud { get; set; }
       
        public string Descripcion { get; set; }

        public DateTime? Fecha_solicitud { get; set; }

        public DateTime Fecha_limite { get; set; }

        public int Time { get; set; }

        public int Id_estado_solicitud { get; set; }

        public List<DetalleSolicitudViewModel> Solicitud { get; set; }

    }
}