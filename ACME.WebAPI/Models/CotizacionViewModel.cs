﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ACME.Entities;

namespace ACME.WebAPI.Models
{
    public class CotizacionViewModel
    {
        public int Id_cotizacion { get; set; }
        public int Id_solicitud { get; set; }

        public int Id_proveedor { get; set; }

        public List<DetalleCotizacionViewModel> Cotizacion { get; set; }

    }
}