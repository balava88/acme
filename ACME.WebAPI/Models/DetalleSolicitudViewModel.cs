﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ACME.Entities;

namespace ACME.WebAPI.Models
{
    public class DetalleCotizacionViewModel
    {
        public int Id_producto { get; set; }

        public decimal Precio { get; set; }
    }
}