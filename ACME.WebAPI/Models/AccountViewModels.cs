﻿using System;
using System.Collections.Generic;

namespace ACME.WebAPI.Models
{
    // Models returned by AccountController actions.

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class SRYCRUserInfoViewModel
    {
       
        public string FirstName { get; set; }       
        public string LastName { get; set; }      
        public string DocumentNumber { get; set; }       
        public decimal? IdApb { get; set; }       
        public string ApbName { get; set; }
        public bool IsLockOut { get; set; }
        public string UserRoles { get; set; }   
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }
}
