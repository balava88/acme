﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ACME.WebAPI.Models
{
    // Models used as parameters to AccountController actions.

    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }



    public class ActivateUserBindingModel
    {
        [Required(ErrorMessage = "El Id del usuario es requerido.")]   
        public string UserId { get; set; }      
    }

    public class ChangePasswordBindingModel
    {
        [Required (ErrorMessage= "La contraseña actual es requerida.")]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required (ErrorMessage = "La nueva contraseña es requerida.")]
        [StringLength(100, ErrorMessage = "La nueva contraseña debe tener por lo menos 6 caracteres de longitud.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "La nueva contraseña y su confirmación no concuerdan.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterBindingModel
    {

        [Required]
        [Display(Name = "First Name")]
        [DataType(DataType.Text)]
        [StringLength(20, ErrorMessage = "El nombre debe tener máximo 20 caracteres")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [DataType(DataType.Text)]
        [StringLength(20, ErrorMessage = "El apellido debe tener máximo 20 caracteres")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Documento")]       
        [StringLength(12, ErrorMessage = "El documento debe tener máximo 12 caracteres")]
        public string Document { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage="Ingrese una dirección de correo valida")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "La contraseña de contener minimo 6 caracteres", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "La contraseña y la confirmación de la contraseña deben conincidir")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterExternalBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }

    public class SetPasswordBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Ingrese una dirección de correo valida")]
        public string Email { get; set; }
    }
}
