﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ACME.Entities;

namespace ACME.WebAPI.Models
{
    public class ProveedorViewModel
    {
        public int Id_proveedor { get; set; }

        [StringLength(150)]
        public string Nombre { get; set; }

        [StringLength(50)]
        public string Nit { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Telefono { get; set; }

        [StringLength(50)]
        public string Direccion { get; set; }

        public List<int> Productos { get; set; }

    }
}