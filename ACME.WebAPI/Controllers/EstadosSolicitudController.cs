﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.Description;
using System.Web.OData;
using ACME.Data.Contracts.Contracts;
using ACME.Data.Repositories;
using ACME.Entities;

namespace ACME.WebAPI.Controllers
{
    public class EstadosSolicitudController : ApiController
    {
        IRepositoryEstadoSolicitud _repositoryEstadoSolicitud;


        public EstadosSolicitudController()
        {
            _repositoryEstadoSolicitud = new RepositoryEstadoSolicitud();
        }


        // GET: api/EstadosSolicitud
        [EnableQuery()]
        [ResponseType(typeof(EstadoSolicitud))]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(_repositoryEstadoSolicitud.Get().AsQueryable());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


    }
}