﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using ACME.Data.Contracts.Contracts;
using Microsoft.AspNet.Identity;
using ACME.WebAPI.Models;
using ACME.Data.Contracts;
using ACME.Data;
using System.Web.Http.Description;
using System.Web.OData;
using ACME.Data.Repositories;
using ACME.Entities;

namespace ACME.WebAPI.Controllers
{

    public class SolicitudesController : ApiController
    {
        readonly IRepositorySolicitud _repositorySolicitud;
        readonly IRepositoryDetalleSolicitud _repositoryDetalleSolicitud;

        public SolicitudesController()
        {
            _repositorySolicitud = new RepositorySolicitud();
            _repositoryDetalleSolicitud = new RepositoryDetalleSolicitud();

        }


        // GET: api/Solicitudes
        [Authorize]
        [EnableQuery()]
        [ResponseType(typeof(SolicitudViewModel))]
        public IHttpActionResult Get()
        {
            try
            {
                var userId = User.Identity.GetUserId();

                List<Solicitud> solicitudes = userId != null ? _repositorySolicitud.GetSolicitudesPorUsuario(new Guid(userId)) : _repositorySolicitud.GetSolicitudesCompletas();
                
                var newSolicitudes = new List<SolicitudViewModel>();

                foreach (var solicitud in solicitudes)
                {
                    var newSolicitud = new SolicitudViewModel
                    {
                        Id_solicitud = solicitud.Id_solicitud,
                        Fecha_limite = solicitud.Fecha_limite,
                        Fecha_solicitud = solicitud.Fecha_solicitud,
                        Descripcion = solicitud.Descripcion,
                        Id_estado_solicitud = solicitud.Id_estado_solicitud,
                        Solicitud = new List<DetalleSolicitudViewModel>()
                    };

                    foreach (var detalle in solicitud.DetalleSolicitud)
                    {
                        var newDetalle = new DetalleSolicitudViewModel
                        {
                            Id_producto = detalle.Id_producto,
                            Cantidad = detalle.Cantidad
                        };

                        newSolicitud.Solicitud.Add(newDetalle);
                    }

                    newSolicitudes.Add(newSolicitud);
                }

                return Ok(newSolicitudes); 
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        // GET: api/Solicitudes/5
        [ResponseType(typeof(SolicitudViewModel))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                Solicitud solicitud;

                if (id > 0)
                {

                    solicitud = _repositorySolicitud.GetSolicitudCompleta(id);

                    if (solicitud == null)
                    {
                        return NotFound();
                    }
                }
                else
                {
                    solicitud = new Solicitud() { Id_solicitud = 0 };
                }


                var newSolicitud = new SolicitudViewModel
                {
                    Id_solicitud = solicitud.Id_solicitud,
                    Fecha_limite = solicitud.Fecha_limite,
                    Fecha_solicitud = solicitud.Fecha_solicitud,
                    Descripcion = solicitud.Descripcion,
                    Id_estado_solicitud = solicitud.Id_estado_solicitud,
                    Solicitud = new List<DetalleSolicitudViewModel>()
                };

                foreach (var detalle in solicitud.DetalleSolicitud)
                {
                    var newDetalle = new DetalleSolicitudViewModel
                    {
                        Id_producto = detalle.Id_producto,
                        Cantidad = detalle.Cantidad
                    };

                    newSolicitud.Solicitud.Add(newDetalle);
                }

                return Ok(newSolicitud);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        // POST: api/Solicitudes
        [Authorize]
        [ResponseType(typeof(SolicitudViewModel))]
        public IHttpActionResult Post([FromBody]SolicitudViewModel solicitud)
        {
            try
            {
                if (solicitud == null)
                {
                    return BadRequest("La solicitud no puede ser nula");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var newSolicitud = new Solicitud
                {
                    Fecha_solicitud = DateTime.Now,
                    Fecha_limite = DateTime.Now.AddMinutes(solicitud.Time),
                    Descripcion = solicitud.Descripcion,
                    Id_cliente = 1, //borrar
                    Id_estado_solicitud = 1,//Estado Nueva
                    Usuario = new Guid(User.Identity.GetUserId()) //new Guid("d0ddf08c-5e78-456c-8caf-5ba21e23cda9") 
                };

                var createdSolicitud = _repositorySolicitud.Insert(newSolicitud);

                if (createdSolicitud == null)
                {
                    return Conflict();
                }

                foreach (var detalle in solicitud.Solicitud)
                {
                    var newDetalle = new DetalleSolicitud
                    {
                        Id_solicitud = createdSolicitud.Id_solicitud,
                        Id_producto = detalle.Id_producto,
                        Cantidad = detalle.Cantidad
                    };

                    var createdDetalleSolicitud = _repositoryDetalleSolicitud.Insert(newDetalle);

                    if (createdDetalleSolicitud == null)
                    {
                        return Conflict();
                    }
                }

                solicitud.Id_solicitud = createdSolicitud.Id_solicitud;

                return Created(Request.RequestUri + "/" + solicitud.Id_solicitud.ToString(CultureInfo.InvariantCulture),
                    solicitud);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        

    }
}