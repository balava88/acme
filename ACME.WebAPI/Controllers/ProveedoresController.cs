﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.Description;
using System.Web.OData;
using ACME.Data.Contracts;
using ACME.Data.Contracts.Contracts;
using ACME.Data.Repositories;
using ACME.Entities;
using ACME.WebAPI.Models;

namespace ACME.WebAPI.Controllers
{
    public class ProveedoresController : ApiController
    {
        readonly IRepositoryProveedor _repositoryProveedor;
        IRepositorySuscripcion _repositorySuscripcion;


        public ProveedoresController()
        {
            _repositoryProveedor = new RepositoryProveedor();
            _repositorySuscripcion = new RepositorySuscripcion();
        }


        // GET: api/Proveedores
        [ResponseType(typeof(ProveedorViewModel))]
        public IHttpActionResult Get()
        {
            try
            {
                var newProveedores = new List<ProveedorViewModel>();

                var proveedores = _repositoryProveedor.Get();

                foreach (var proveedor in proveedores)
                {
                    var newProveedor = new ProveedorViewModel
                    {
                        Id_proveedor = proveedor.Id_proveedor,
                        Nombre = proveedor.Nombre,
                        Nit = proveedor.Nit,
                        Email = proveedor.Email,
                        Telefono = proveedor.Telefono,
                        Direccion = proveedor.Direccion
                    };

                    var suscripciones = _repositorySuscripcion.GetSuscripcionesPorProveedor(newProveedor.Id_proveedor);
                    newProveedor.Productos = suscripciones.Select(suscripcion => suscripcion.Id_producto).ToList();

                    newProveedores.Add(newProveedor);
                }

                return Ok(newProveedores);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        // GET: api/Proveedores/5
        [ResponseType(typeof(ProveedorViewModel))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                Proveedor proveedor;

                if (id > 0)
                {

                    proveedor = _repositoryProveedor.Get(id);

                    if (proveedor == null)
                    {
                        return NotFound();
                    }
                }
                else
                {
                    proveedor = new Proveedor() { Id_proveedor = 0 };
                }

                var newProveedor = new ProveedorViewModel
                {
                    Id_proveedor = proveedor.Id_proveedor,
                    Nombre = proveedor.Nombre,
                    Nit = proveedor.Nit,
                    Email = proveedor.Email,
                    Telefono = proveedor.Telefono,
                    Direccion = proveedor.Direccion
                };

                var suscripciones = _repositorySuscripcion.GetSuscripcionesPorProveedor(newProveedor.Id_proveedor);
                newProveedor.Productos = suscripciones.Select(suscripcion => suscripcion.Id_producto).ToList();

                return Ok(newProveedor);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        // POST: api/Proveedores
        [ResponseType(typeof(ProveedorViewModel))]
        public IHttpActionResult Post([FromBody]ProveedorViewModel proveedor)
        {
            try
            {
                if (proveedor == null)
                {
                    return BadRequest("El proveedor no puede ser nulo");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var newProveedor = new Proveedor
                {
                    Id_proveedor = 0,
                    Telefono = proveedor.Telefono,
                    Nit = proveedor.Nit,
                    Nombre = proveedor.Nombre,
                    Email = proveedor.Email,
                    Direccion = proveedor.Direccion
                };

                var nuevoProveedor = _repositoryProveedor.Insert(newProveedor);

                if (nuevoProveedor == null)
                {
                    return Conflict();
                }
               

                //Inserta las suscripciones
                foreach (var productId in proveedor.Productos)
                {
                    var suscripcion = new Suscripcion
                    {
                        Id_producto = productId,
                        Id_proveedor = nuevoProveedor.Id_proveedor
                    };

                    var newSuscripcion =  _repositorySuscripcion.Insert(suscripcion);

                    if (newSuscripcion == null)
                    {
                        return Conflict();
                    }
                }


                return Created(Request.RequestUri + "/" + nuevoProveedor.Id_proveedor.ToString(CultureInfo.InvariantCulture),
                    nuevoProveedor);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }




        //// PUT: api/Proveedores/5
     
        [HttpPut]
        [AcceptVerbs("PUT")]
        public IHttpActionResult Put(int id, [FromBody]ProveedorViewModel proveedor)
        {
            try
            {
                if (proveedor == null)
                {
                    return BadRequest("El proveedor no puede ser nulo");
                }
                if (id != proveedor.Id_proveedor)
                {
                    return BadRequest("Error en el identificator del proveedor");
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var newProveedor = new Proveedor
                {
                    Id_proveedor = proveedor.Id_proveedor,
                    Telefono = proveedor.Telefono,
                    Nit = proveedor.Nit,
                    Nombre = proveedor.Nombre,
                    Email = proveedor.Email,
                    Direccion = proveedor.Direccion
                };

                var updatedProveedor = _repositoryProveedor.Update(newProveedor);

                if (updatedProveedor == null)
                {
                    return NotFound();
                }


                //Borra todas las suscripciones
                _repositorySuscripcion.DeleteSuscripcionesPorProveedor(updatedProveedor.Id_proveedor);


                foreach (var productId in proveedor.Productos)
                {
                    var suscripcion = new Suscripcion
                    {
                        Id_producto = productId,
                        Id_proveedor = updatedProveedor.Id_proveedor
                    };

                    var newSuscripcion = _repositorySuscripcion.Insert(suscripcion);

                    if (newSuscripcion == null)
                    {
                        return Conflict();
                    }
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // DELETE: api/Products/5
        public void Delete(int id)
        {
           
        }
    }
}