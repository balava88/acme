﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using ACME.Data.Contracts.Contracts;
using Microsoft.AspNet.Identity;
using ACME.WebAPI.Models;
using ACME.Data.Contracts;
using ACME.Data;
using System.Web.Http.Description;
using System.Web.OData;
using ACME.Data.Repositories;
using ACME.Entities;

namespace ACME.WebAPI.Controllers
{

    public class CotizacionesController : ApiController
    {
        readonly IRepositoryCotizacion _repositoryCotizacion;
        readonly IRepositoryDetalleCotizacion _repositoryDetalleCotizacion;

        public CotizacionesController()
        {
            _repositoryCotizacion = new RepositoryCotizacion();
            _repositoryDetalleCotizacion = new RepositoryDetalleCotizacion();
        }

        // GET: api/proveedores/{id}/cotizaciones
        [Route("api/proveedores/{id}/cotizaciones")]
        [HttpGet]
        [ResponseType(typeof(CotizacionViewModel))]
        public IHttpActionResult CotizacionesPorProveedor(int id)
        {
            try
            {
                List<Cotizacion> cotizaciones = _repositoryCotizacion.GetCotizacionesPorProveedor(id);

                var newCotizaciones = new List<CotizacionViewModel>();

                foreach (var cotizacion in cotizaciones)
                {
                    var newCotizacion = new CotizacionViewModel
                    {
                        Id_solicitud = cotizacion.Id_solicitud,
                        Id_cotizacion = cotizacion.Id_cotizacion,
                        Id_proveedor = cotizacion.Id_proveedor,
                        Cotizacion = new List<DetalleCotizacionViewModel>()
                    };

                    foreach (var detalle in cotizacion.DetalleCotizacion)
                    {
                        var newDetalle = new DetalleCotizacionViewModel
                        {
                            Id_producto = detalle.Id_producto,
                            Precio = detalle.Precio
                        };

                        newCotizacion.Cotizacion.Add(newDetalle);
                    }

                    newCotizaciones.Add(newCotizacion);
                }

                return Ok(newCotizaciones);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        // GET: api/Cotizaciones/5
        [ResponseType(typeof(CotizacionViewModel))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                Cotizacion cotizacion;

                if (id > 0)
                {
                    cotizacion = _repositoryCotizacion.GetCotizacionCompleta(id);

                    if (_repositoryCotizacion == null)
                    {
                        return NotFound();
                    }
                }
                else
                {
                    cotizacion = new Cotizacion() { Id_cotizacion = 0 };
                }


                var newCotizacion = new CotizacionViewModel
                {
                    Id_solicitud = cotizacion.Id_solicitud,
                    Id_cotizacion = cotizacion.Id_cotizacion,
                    Id_proveedor = cotizacion.Id_proveedor,
                    Cotizacion = new List<DetalleCotizacionViewModel>()
                };

                foreach (var detalle in cotizacion.DetalleCotizacion)
                {
                    var newDetalle = new DetalleCotizacionViewModel
                    {
                        Id_producto = detalle.Id_producto,
                        Precio = detalle.Precio
                    };

                    newCotizacion.Cotizacion.Add(newDetalle);
                }

                return Ok(newCotizacion);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }






        // POST: api/Cotizaciones
        [ResponseType(typeof(CotizacionViewModel))]
        public IHttpActionResult Post([FromBody]CotizacionViewModel cotizacion)
        {
            try
            {
                if (cotizacion == null)
                {
                    return BadRequest("La cotizacion no puede ser nula");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var newSolicitud = new Cotizacion()
                {
                    Id_proveedor = cotizacion.Id_proveedor,
                    Id_solicitud = cotizacion.Id_solicitud,
                    Id_estado_cotizacion = 1 //Estado Nueva
                };

                var createdCotizacion = _repositoryCotizacion.Insert(newSolicitud);

                if (createdCotizacion == null)
                {
                    return Conflict();
                }

                foreach (var detalle in cotizacion.Cotizacion)
                {
                    var newDetalle = new DetalleCotizacion()
                    {
                        Id_cotizacion = createdCotizacion.Id_cotizacion,
                        Id_producto = detalle.Id_producto,
                        Precio = detalle.Precio
                    };

                    var createdDetalleCotizacion = _repositoryDetalleCotizacion.Insert(newDetalle);

                    if (createdDetalleCotizacion == null)
                    {
                        return Conflict();
                    }
                }

                cotizacion.Id_cotizacion = createdCotizacion.Id_cotizacion;

                return Created(Request.RequestUri + cotizacion.Id_cotizacion.ToString(CultureInfo.InvariantCulture),
                    cotizacion);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }



    }
}