﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.Description;
using System.Web.OData;
using ACME.Data.Contracts.Contracts;
using ACME.Data.Repositories;
using ACME.Entities;

namespace ACME.WebAPI.Controllers
{
    public class ProductosController : ApiController
    {
        IRepositoryProducto _repositoryProducto;
       

        public ProductosController()
        {
            _repositoryProducto = new RepositoryProducto();
        }


        // GET: api/Productos
        [EnableQuery()]
        [ResponseType(typeof(Producto))]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(_repositoryProducto.Get().AsQueryable());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        // GET: api/Productos/5
        [ResponseType(typeof(Producto))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                Producto producto;

                if (id > 0)
                {

                    producto = _repositoryProducto.Get(id);

                    if (producto == null)
                    {
                        return NotFound();
                    }
                }
                else
                {
                    producto = new Producto() { Id_producto = 0 };
                }
                return Ok(producto);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        // POST: api/Productos
        [ResponseType(typeof(Producto))]
        public IHttpActionResult Post([FromBody]Producto producto)
        {
            try
            {
                if (producto == null)
                {
                    return BadRequest("El producto  no  puede estar vacío");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var nuevoProducto = _repositoryProducto.Insert(producto);
                if (nuevoProducto == null)
                {
                    return Conflict();
                }
                return Created(Request.RequestUri + "/" + nuevoProducto.Id_producto.ToString(CultureInfo.InvariantCulture),
                    nuevoProducto);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }




        // PUT: api/Productos/5
        public IHttpActionResult Put(int id, [FromBody]Producto producto)
        {
            try
            {
                if (producto == null)
                {
                    return BadRequest("El poducto no puede ser nulo");
                }
                if (id != producto.Id_producto)
                {
                    return BadRequest("Error en el identificator del producto");
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }


                var updatedProducto = _repositoryProducto.Update(producto);
                if (updatedProducto == null)
                {
                    return NotFound();
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // DELETE: api/Products/5
        public void Delete(int id)
        {
           
        }
    }
}