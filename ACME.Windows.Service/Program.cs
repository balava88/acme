﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using ACME.Business;
using ACME.Business.Contracts;

namespace ACME.Windows.Service
{
    class Program
    {

        private static Timer _t = null;

        static void Main(string[] args)
        {
            Console.WriteLine("Service is Up");

            Proceso();

            _t = new Timer(5000); //5 sec


            _t.Elapsed += t_Elapsed;

            _t.Start();

            Console.ReadKey();

            _t.Stop();

            Console.WriteLine("Service is down");
            Console.WriteLine("Press any key to exit");

            Console.ReadKey();

        }

        static void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            // stop the timer so we can do the process
            _t.Stop();

            Proceso();

            //after we finish with the file, we start the time again..
            _t.Start();
        }


        private static void Proceso()
        {
            try
            {
                //Revisar nuevas solicitudes
                //Notificar proveedores sobre nuevas solicitudes por tipo de producto (Topics)
                NotificarProveedores();


                //Revisar solicitudes vencidas
                //Generar mejor cotización para solicitudes vencidas
                //Notificar clientes sobre mejor cotización para solicitudes vencidas

                NotificarClientes();


            }
            catch (Exception ex)
            {
                if (!Directory.Exists(@"C:\error\"))
                {
                    Directory.CreateDirectory(@"C:\error\");
                }
                TextWriter tw = new StreamWriter(@"C:\error\Log.txt", true);
                tw.WriteLine("Error : " + DateTime.Now.ToString(CultureInfo.InvariantCulture) + ", Error en t_Elapsed : " + ex.Message.ToString(CultureInfo.InvariantCulture));
                tw.Close();
            }
        }

        private static void NotificarClientes()
        {
            IACMEEnginee busines = new ACMEEnginee();

            busines.NotificarClientes();
        }

        private static void NotificarProveedores()
        {

            IACMEEnginee busines = new ACMEEnginee();

            busines.NotificarProveedores();


        }
    }
}
