namespace ACME.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetalleCotizacion")]
    public partial class DetalleCotizacion
    {
        [Key]
        public int Id_detalle_cotizacion { get; set; }

        public int Id_cotizacion { get; set; }

        public int Id_producto { get; set; }

        [Column(TypeName = "money")]
        public decimal Precio { get; set; }

        //public virtual Cotizacion Cotizacion { get; set; }

        //public virtual Producto Producto { get; set; }
    }
}
