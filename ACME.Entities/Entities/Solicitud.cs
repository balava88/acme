namespace ACME.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Solicitud")]
    public partial class Solicitud
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Solicitud()
        {
            //Cotizacion = new HashSet<Cotizacion>();
            DetalleSolicitud = new HashSet<DetalleSolicitud>();
        }

        [Key]
        public int Id_solicitud { get; set; }

        [StringLength(150)]
        public string Descripcion { get; set; }

        public DateTime? Fecha_solicitud { get; set; }

        public int Id_cliente { get; set; }

        public DateTime Fecha_limite { get; set; }

        public int Id_estado_solicitud { get; set; }

        //public virtual Cliente Cliente { get; set; }

        public Guid? Usuario { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Cotizacion> Cotizacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetalleSolicitud> DetalleSolicitud { get; set; }

        //public virtual EstadoSolicitud EstadoSolicitud { get; set; }
    }
}
