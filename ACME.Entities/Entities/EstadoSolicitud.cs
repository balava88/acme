namespace ACME.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EstadoSolicitud")]
    public partial class EstadoSolicitud
    {
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        //public EstadoSolicitud()
        //{
        //    Solicitud = new HashSet<Solicitud>();
        //}

        [Key]
        public int Id_estado_solicitud { get; set; }

        [StringLength(50)]
        public string Nombre_estado { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Solicitud> Solicitud { get; set; }
    }
}
