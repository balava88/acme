namespace ACME.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetalleSolicitud")]
    public partial class DetalleSolicitud
    {
        [Key]
        public int Id_detalle_solitud { get; set; }

        public int Id_solicitud { get; set; }

        public int Id_producto { get; set; }

        public int Cantidad { get; set; }

        public int? Id_mejor_proveedor { get; set; }

        [Column(TypeName = "money")]
        public decimal? Precio_mejor_proveedor { get; set; }

        [StringLength(500)]
        public string Datos_mejor_proveedor { get; set; }
     
    }
}
