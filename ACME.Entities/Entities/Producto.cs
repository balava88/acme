namespace ACME.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Producto")]
    public partial class Producto
    {
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        //public Producto()
        //{
        //    DetalleCotizacion = new HashSet<DetalleCotizacion>();
        //    DetalleSolicitud = new HashSet<DetalleSolicitud>();
        //}

        [Key]
        public int Id_producto { get; set; }

        [StringLength(50)]
        public string Nombre_producto { get; set; }

        [StringLength(150)]
        public string Descripcion { get; set; }

        [StringLength(50)]
        public string Codigo { get; set; }

        [StringLength(150)]
        public string Imagen { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<DetalleCotizacion> DetalleCotizacion { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<DetalleSolicitud> DetalleSolicitud { get; set; }
    }
}
