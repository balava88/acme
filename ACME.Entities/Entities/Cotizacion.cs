namespace ACME.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cotizacion")]
    public partial class Cotizacion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cotizacion()
        {
            DetalleCotizacion = new HashSet<DetalleCotizacion>();
        }

        [Key]
        public int Id_cotizacion { get; set; }

        public int Id_solicitud { get; set; }

        public int Id_proveedor { get; set; }

        public int Id_estado_cotizacion { get; set; }

        public virtual EstadoCotizacion EstadoCotizacion { get; set; }

        public virtual Proveedor Proveedor { get; set; }

        public virtual Solicitud Solicitud { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetalleCotizacion> DetalleCotizacion { get; set; }
    }
}
