namespace ACME.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Suscripcion")]
    public partial class Suscripcion
    {
        [Key]
        public int Id_suscripcion { get; set; }

        public int Id_producto { get; set; }

        public int Id_proveedor { get; set; }
    }
}
