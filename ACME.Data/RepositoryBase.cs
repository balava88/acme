﻿using Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME.Data
{
    public abstract class RepositoryBase<E> : RepositoryBase<E,ACMEContext>
        where E : class, new()
    {
    }
}
