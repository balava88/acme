﻿using ACME.Data.Contracts;
using ACME.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME.Data
{
    public class RepositoryUser : RepositoryBase<AspNetUsers>, IRepositoryUser
    {

        public AspNetUsers getUserByUserName(string userName)
        {
            AspNetUsers user;
            using(ACMEContext _context = new ACMEContext()){
                user = (from x in _context.AspNetUsers
                        where x.UserName == userName
                        select x).SingleOrDefault();
            }
            return user;
        }


        public AspNetUsers getUserByUserId(string userId)
        {
            AspNetUsers user;
            using (ACMEContext _context = new ACMEContext())
            {
                user = (from x in _context.AspNetUsers
                        where x.Id == userId
                        select x).SingleOrDefault();
            }
            return user;
        }
    }
}
