﻿using System.Data.Entity;
using ACME.Data.Contracts;
using ACME.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACME.Data.Contracts.Contracts;

namespace ACME.Data.Repositories
{
    public class RepositorySuscripcion : RepositoryBase<Suscripcion>, IRepositorySuscripcion
    {
        public void DeleteSuscripcionesPorProveedor(int idProveedor)
        {
            List<Suscripcion> suscripciones;

            using (var context = new ACMEContext())
            {
                suscripciones = (from x in context.Suscripcion
                                 where x.Id_proveedor == idProveedor
                                 select x).ToList();

              
                context.Suscripcion.RemoveRange(suscripciones);

                context.SaveChanges();

            }
            
        }

        public List<Proveedor> GetProveedoresPorSuscripciones(int idProducto)
        {
            var proveedores = new List<Proveedor>();

            using (var context = new ACMEContext())
            {
                List<Suscripcion> suscripciones = (from x in context.Suscripcion
                    where x.Id_producto == idProducto
                    select x).ToList();

                proveedores.AddRange(suscripciones.Select(suscripcion => (from x in context.Proveedores where x.Id_proveedor == suscripcion.Id_proveedor select x).SingleOrDefault()));
            }

            return proveedores;
        }


        public List<Suscripcion> GetSuscripcionesPorProveedor(int idProveedor)
        {
            List<Suscripcion> suscripciones;

            using (var context = new ACMEContext())
            {
                suscripciones = (from x in context.Suscripcion
                                 where x.Id_proveedor == idProveedor
                                 select x).ToList();
            }

            return suscripciones;

        }
    }
}
