﻿using System.Data.Entity;
using ACME.Data.Contracts;
using ACME.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACME.Data.Contracts.Contracts;
using Core.Common;

namespace ACME.Data.Repositories
{
    public class RepositoryCotizacion : RepositoryBase<Cotizacion>, IRepositoryCotizacion
    {

        public List<Cotizacion> GetCotizacionesPorProveedor(int proveedorId)
        {
            List<Cotizacion> cotizaciones;

            using (var context = new ACMEContext())
            {
                cotizaciones = (from x in context.Cotizaciones.Include(x => x.DetalleCotizacion)
                               where x.Id_proveedor == proveedorId
                               select x).ToList();
            }

            return cotizaciones;
        }

        public Cotizacion GetCotizacionCompleta(int id)
        {
            Cotizacion cotizacion;

            using (var context = new ACMEContext())
            {
                cotizacion = (from x in context.Cotizaciones.Include(x => x.DetalleCotizacion)
                             where x.Id_cotizacion == id
                             select x).SingleOrDefault();
            }
            return cotizacion;
        }

        public List<Cotizacion> GetCotizacionesCompletasPorSolicitud(int idSolicitud)
        {
            List<Cotizacion> cotizaciones;

            using (var context = new ACMEContext())
            {
                cotizaciones = (from x in context.Cotizaciones.Include(x => x.DetalleCotizacion)
                              where x.Id_solicitud == idSolicitud
                              select x).ToList();
            }
            return cotizaciones;
        }

    }
}
