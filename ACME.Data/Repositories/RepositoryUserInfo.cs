﻿using ACME.Data.Contracts;
using ACME.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME.Data
{
    public class RepositoryUserInfo : RepositoryBase<userInfo>, IRepositoryUserInfo
    {
        public userInfo getUserProfileByUserId(string userId)
        {
            userInfo user;
            using (ACMEContext _context = new ACMEContext())
            {
                user = (from x in _context.userInfo
                        where x.Id == userId
                        select x).SingleOrDefault();
            }
            return user;
        }

    }
}
