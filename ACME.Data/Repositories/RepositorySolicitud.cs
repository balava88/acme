﻿using System.Data.Entity;
using ACME.Data.Contracts;
using ACME.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME.Data.Repositories
{
    public class RepositorySolicitud : RepositoryBase<Solicitud>, IRepositorySolicitud
    {
        public List<Solicitud> GetSolicitudesPorUsuario(Guid userId)
        {
            List<Solicitud> solicitudes;

            using (var context = new ACMEContext())
            {
                solicitudes = (from x in context.Solicitudes.Include(x => x.DetalleSolicitud)
                               where x.Usuario == userId 
                               select x).ToList();
            }
            
            return solicitudes;
        }

    

        public Solicitud GetSolicitudCompleta(int id)
        {
            Solicitud solicitud;

            using (var context = new ACMEContext())
            {
                solicitud = (from x in context.Solicitudes.Include(x => x.DetalleSolicitud)
                    where x.Id_solicitud == id
                    select x).SingleOrDefault();
            }
            return solicitud;
        }


        public List<Solicitud> GetSolicitudesCompletas()
        {
            List<Solicitud> solicitudes;

            using (var context = new ACMEContext())
            {
                solicitudes = (from x in context.Solicitudes.Include(x => x.DetalleSolicitud)
                               select x).ToList();
            }
            return solicitudes;
        }


        public List<Solicitud> GetNuevasSolicitudesCompletas()
        {
            List<Solicitud> solicitudes;

            using (var context = new ACMEContext())
            {
                solicitudes = (from x in context.Solicitudes.Include(x => x.DetalleSolicitud)
                               where x.Id_estado_solicitud == 1
                               select x).ToList();
            }
            return solicitudes;
        }


        public List<Solicitud> GetSolicitudesEnviadasVencidasCompletas()
        {
            List<Solicitud> solicitudes;

            using (var context = new ACMEContext())
            {
                solicitudes = (from x in context.Solicitudes.Include(x => x.DetalleSolicitud)
                               where x.Id_estado_solicitud == 2 && x.Fecha_limite > DateTime.Now
                               select x).ToList();
            }
            return solicitudes;
        }
    }
}
