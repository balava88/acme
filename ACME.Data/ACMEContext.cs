namespace ACME.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using ACME.Entities;

    public partial class ACMEContext : DbContext
    {
        public ACMEContext()
            : base("name=DefaultConnection")
        {
        }

   
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<userInfo> userInfo { get; set; }
      


        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<Cotizacion> Cotizaciones { get; set; }
        public virtual DbSet<DetalleCotizacion> DetallesCotizacion { get; set; }
        public virtual DbSet<DetalleSolicitud> DetallesSolicitud { get; set; }
        public virtual DbSet<EstadoCotizacion> EstadosCotizacion { get; set; }
        public virtual DbSet<EstadoSolicitud> EstadosSolicitud { get; set; }
        public virtual DbSet<Producto> Productos { get; set; }
        public virtual DbSet<Proveedor> Proveedores { get; set; }
        public virtual DbSet<Solicitud> Solicitudes { get; set; }
        public virtual DbSet<Suscripcion> Suscripcion { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Nombre)
                .IsFixedLength();

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Apellido)
                .IsFixedLength();

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Identificación)
                .IsFixedLength();

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Email)
                .IsFixedLength();

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Telefono)
                .IsFixedLength();

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Direccion)
                .IsFixedLength();

            modelBuilder.Entity<DetalleCotizacion>()
                .Property(e => e.Precio)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DetalleSolicitud>()
                .Property(e => e.Precio_mejor_proveedor)
                .HasPrecision(19, 4);

            modelBuilder.Entity<EstadoCotizacion>()
                .HasMany(e => e.Cotizacion)
                .WithRequired(e => e.EstadoCotizacion)
                .WillCascadeOnDelete(false);

  
        }
    }
}
