﻿using ACME.Data.Contracts.Contracts;
using ACME.Data.Repositories;
using Core.Common.Contracts;
using Core.Common.Data;
using Core.Common;
using ACME.Business.Contracts;
using ACME.Data;
using ACME.Data.Contracts;
using ACME.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME.Business
{
    public class ACMEEnginee : IACMEEnginee
    {
        
        IEmailService _emailService;
      


        public ACMEEnginee()
        {            
            _emailService = new EmailService();
        
        }


      



        #region Clientes

        public void NotificarClientes()
        {

            //Obtiene la lista de las solicitudes enviadas que estan vencidas 

            IRepositorySolicitud repositorySolicitud = new RepositorySolicitud();

            var solicitudes = repositorySolicitud.GetSolicitudesEnviadasVencidasCompletas();

            foreach (var solicitud in solicitudes)
            {
                CalcularMejorProveedorPorSolicitud(solicitud);

                NotificarClientePorSolicitud(solicitud);

                //solicitud.Id_estado_solicitud = 4;

                //repositorySolicitud.Update(solicitud);
            }

        }

        private void NotificarClientePorSolicitud(Solicitud solicitud)
        {
            IRepositorySolicitud repositorySolicitud = new RepositorySolicitud();

            IRepositoryUser repositoryUser = new RepositoryUser();

            var user = repositoryUser.getUserByUserId(solicitud.Usuario.ToString());

            var correos = new List<string> { user.Email };

            var solicitudActualizada = repositorySolicitud.GetSolicitudCompleta(solicitud.Id_solicitud);

            string body = BodyHTML(solicitudActualizada);

            _emailService.SendEmail("contacto@acme.com", correos, "Servicio de solicitudes ACME", body);

        }

        private void CalcularMejorProveedorPorSolicitud(Solicitud solicitud)
        {
            IRepositoryCotizacion repositoryCotizacion = new RepositoryCotizacion();
            IRepositoryDetalleSolicitud repositoryDetalleSolicitud = new RepositoryDetalleSolicitud();

            var cotizaciones = repositoryCotizacion.GetCotizacionesCompletasPorSolicitud(solicitud.Id_solicitud);

            foreach (var detalle in solicitud.DetalleSolicitud)
            {
                decimal bestPrice;
                int idProveedor;
                ObtenerMejorProveedorPorProducto(detalle, cotizaciones, out bestPrice, out idProveedor);

                if (bestPrice != 0 && idProveedor != 0)
                {
                    detalle.Precio_mejor_proveedor = bestPrice;
                    detalle.Id_mejor_proveedor = idProveedor;
                    repositoryDetalleSolicitud.Update(detalle);
                }
            }
        }

        private void ObtenerMejorProveedorPorProducto(DetalleSolicitud detalle, IEnumerable<Cotizacion> cotizaciones, out decimal bestPrice, out int idProveedor)
        {
            bestPrice = 0;
            idProveedor = 0;
            bool isFirst = true;

            foreach (var cotizacion in cotizaciones)
            {
                var temp = cotizacion.DetalleCotizacion.FirstOrDefault(x => x.Id_producto == detalle.Id_producto);
                if (temp != null)
                {
                    if (isFirst)
                    {
                        bestPrice = temp.Precio;
                        idProveedor = cotizacion.Id_proveedor;
                        isFirst = false;
                    }
                    else
                    {
                        if (temp.Precio < bestPrice)
                        {
                            bestPrice = temp.Precio;
                            idProveedor = cotizacion.Id_proveedor;
                        }
                    }

                }

            }

        }

        #endregion


        #region Notifica Proveedores

        public void NotificarProveedores()
        {

            //Obtiene la lista de las nuevas solicitudes creadas en el sistema

            IRepositorySolicitud repositorySolicitud = new RepositorySolicitud();

            var solicitudes = repositorySolicitud.GetNuevasSolicitudesCompletas();

            foreach (var solicitud in solicitudes)
            {
                NotificarProveedoresPorSolicitud(solicitud);

                solicitud.Id_estado_solicitud = 2;

                repositorySolicitud.Update(solicitud);
            }

        }

        private void NotificarProveedoresPorSolicitud(Solicitud solicitud)
        {
            var proveedoresT = new List<Proveedor>();

            foreach (var producto in solicitud.DetalleSolicitud)
            {
                //Buscar todos los proveedores suscritos a ese producto

                IRepositorySuscripcion repositorySuscripcion = new RepositorySuscripcion();

                var proveedores = repositorySuscripcion.GetProveedoresPorSuscripciones(producto.Id_producto);

                proveedoresT.AddRange(proveedores);

            }

            var proveedoresF = proveedoresT.DistinctBy(p => p.Id_proveedor);

            var correos = proveedoresF.Select(proveedor => proveedor.Email).ToList();

            if (correos.Count > 0)
            {
                _emailService.SendEmail("contacto@acme.com", correos, "Servicio de solicitudes ACME", "Se ha creado una nueva solicitud - Numero # " + solicitud.Id_solicitud + "<br/> <br/> Chequee la info aqui:  http://186.29.70.43:8888/api/solicitudes/" + solicitud.Id_solicitud);

            }

        }

        #endregion




        #region HTML E-mails

        private string BodyHTML(Solicitud solicitudActualizada)
        {
            string body = "<h1>Su solicitud Numero # " + solicitudActualizada.Id_solicitud + " ha sido procesada</h1><br/>" +
                "<h2>Mejor cotizacion propuesta</h2><br/>" +
            "<table border='1'>" + 
            "<tr>" +
            "<td><strong>Producto</strong></td>" +
            "<td><strong>Cantidad</strong></td>" +
            "<td><strong>Precio</strong></td>" +
            "<td><strong>Proveedor</strong></td>" +
            "</tr>";

            foreach (var detalle in solicitudActualizada.DetalleSolicitud)
            {
                body = body + "<tr><td>&nbsp;Producto # " + detalle.Id_producto + "</td>" +
                       "<td>" + detalle.Cantidad + "</td>" +
                       "<td>" + (detalle.Precio_mejor_proveedor == null ? "no disponible" : "$" + Convert.ToInt32(detalle.Precio_mejor_proveedor)) + "</td>" +
                       "<td>" + (detalle.Id_mejor_proveedor == null ? "no disponible" : "Proveedor #" + detalle.Id_mejor_proveedor) + "</td></tr>";
            }

            body = body + "</table>";

            return body;
        }
        
      

        #endregion
    }
}
