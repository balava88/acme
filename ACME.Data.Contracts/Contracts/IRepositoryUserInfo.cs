﻿using Core.Common;
using ACME.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME.Data.Contracts
{
    public interface IRepositoryUserInfo : IRepositoryBase<userInfo>
    {
        userInfo getUserProfileByUserId(string userId);
    }
}
