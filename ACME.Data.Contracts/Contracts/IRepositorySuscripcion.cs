﻿using Core.Common;
using ACME.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ACME.Data.Contracts.Contracts
{
    public interface IRepositorySuscripcion : IRepositoryBase<Suscripcion>
    {
        void DeleteSuscripcionesPorProveedor(int idProveedor);

        List<Suscripcion> GetSuscripcionesPorProveedor(int idProveedor);

        List<Proveedor> GetProveedoresPorSuscripciones(int idProducto);
    }
}
