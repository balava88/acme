﻿using Core.Common;
using ACME.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ACME.Data.Contracts.Contracts
{
    public interface IRepositoryCotizacion : IRepositoryBase<Cotizacion>
    {
        List<Cotizacion> GetCotizacionesPorProveedor(int proveedorId);

        Cotizacion GetCotizacionCompleta(int id);

        List<Cotizacion> GetCotizacionesCompletasPorSolicitud(int idSolicitud);
    }
}
