﻿using Core.Common;
using ACME.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME.Data.Contracts
{
    public interface IRepositorySolicitud  : IRepositoryBase<Solicitud>
    {

        List<Solicitud> GetSolicitudesPorUsuario(Guid userId);

        Solicitud GetSolicitudCompleta(int id);

        List<Solicitud> GetSolicitudesCompletas();

        List<Solicitud> GetNuevasSolicitudesCompletas();

        List<Solicitud> GetSolicitudesEnviadasVencidasCompletas();

    }
}
