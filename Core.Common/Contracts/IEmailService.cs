﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Common.Contracts
{
    public interface IEmailService
    {
        void SendEmail(string from, List<string> to, string subject, string body, bool isBodyHtml = true, List<string> cc = null);
    }
}
