﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Common
{
    public interface IRepositoryBase<E>
         where E : class, new()
    {

        E Get(int id);
        List<E> Get();
        E Insert(E newEntity);
        void Delete(int id);
        E Update(E entity);
    }
}
