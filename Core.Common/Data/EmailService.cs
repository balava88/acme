﻿using Core.Common.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Core.Common.Data
{
    public class EmailService : IEmailService
    {

        public void SendEmail(string from, List<string> to, string subject, string body, bool isBodyHtml = true, List<string> cc = null)
        {         
            var myMailMessage = new MailMessage();
            myMailMessage.From = new MailAddress(from);

            foreach (var email in to)
            {
                if (!string.IsNullOrEmpty(email))
                    myMailMessage.To.Add(email);
            }

            if (cc != null)
            {
                foreach (var email in cc)
                {
                    if (!string.IsNullOrEmpty(email))
                        myMailMessage.CC.Add(email);
                }           
            }       
           
            myMailMessage.Subject = subject;
            myMailMessage.IsBodyHtml = isBodyHtml;
            myMailMessage.Body = body;
            var mySmtpClient = new SmtpClient();
            mySmtpClient.Send(myMailMessage);            
        }
    }
}
