﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Core.Common
{
    public abstract class RepositoryBase<E, C> : IRepositoryBase<E>
        where E : class, new()
        where C : DbContext, new()
    {

        public E Get(int id)
        {
            using (C context = new C())
            {
                return context.Set<E>().Find(id);
            }
        }

        public List<E> Get()
        {
            using (C context = new C())
            {
                return context.Set<E>().ToList();
            }
        }

        public E Insert(E newEntity)
        {
            using (C context = new C())
            {
                context.Entry<E>(newEntity).State = EntityState.Added;
                context.SaveChanges();
                return newEntity;
            }
        }

        public void Delete(int id)
        {

            using (C context = new C())
            {
                E entity = context.Set<E>().Find(id);
                context.Entry<E>(entity).State = EntityState.Deleted;
                context.SaveChanges();

            }
        }

        public E Update(E entity)
        {
            using (C context = new C())
            {
                context.Entry<E>(entity).State = EntityState.Modified;
                context.SaveChanges();
                return entity;
            }
        }
    }
}

